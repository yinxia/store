/*
package com.store.interceptor;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.spec.SecretKeySpec;
import java.io.Serializable;
import java.security.Key;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JWT implements Serializable {

	private static final long serialVersionUID = -8432095286352071216L;

	protected static final String JWT_SECRET = "eyJhbGciOiJIUzI1NiJ9";

	public static final int TTL_MILLIS = 60 * 60 * 8;

	*/
/**
	 * 创建jwt
	 * 
	 * @param phone
	 * @return
	 * @throws Exception
	 *//*

	public static String createJWT(Long id,String phone, String password) {
		return createJWT(id,phone,password ,TTL_MILLIS);
	}

	*/
/**
	 * 创建jwt
	 * 
	 * @param phone
	 * @param password
	 * @param ttlMillis
	 *            过期时间，秒
	 * @return
	 * @throws Exception
	 *//*

	public static String createJWT(Long id,String phone, String password, int ttlMillis) {
		*/
/**jwt过期日期计算*//*

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.SECOND, ttlMillis);
		*/
/*写到jwt数据到客户端*//*

		Map<String, Object> claims = new HashMap<String, Object>();
		claims.put("id",id);
		claims.put("phone", phone);
		claims.put("password", password);
		*/
/*设置header*//*

		Map<String, Object> header = new HashMap<String, Object>();
		header.put("typ", "JWT");
		
		String compactJws = Jwts.builder().setHeader(header) 
				.setClaims(claims).signWith(SignatureAlgorithm.HS512, KEY)
				.setExpiration(calendar.getTime()).compact();

		return compactJws;
	}

	protected static final Key KEY = new SecretKeySpec(JWT_SECRET.getBytes(), SignatureAlgorithm.HS512.getJcaName());

	public static Map<String, Object> parseToken(String token) {
		Jws<Claims> claimsJws = Jwts.parser().setSigningKey(KEY).parseClaimsJws(token);
		Claims body = claimsJws.getBody();
		return body ;
	}

	public static void main(String[] args) {
		
		String token = createJWT(111L,"ssss", "admin");
		
		System.out.println(parseToken(token));
	}
}
*/
