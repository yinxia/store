/*
package com.store.interceptor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.store.id.DateJsonSerializer;
import com.store.id.EnumJsonSerializer;
import com.store.id.LongJsonDeserializer;
import com.store.id.LongJsonSerializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Configuration
public class MyWebAppConfigurer extends WebMvcConfigurerAdapter {

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LoginInterceptor()).addPathPatterns("/**");
	}

	*/
/**
	 * 新增枚举转换器 json返回枚举数字
	 *//*

	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {

		Iterator<HttpMessageConverter<?>> iterator = converters.iterator();
	    while(iterator.hasNext()){
	        HttpMessageConverter<?> converter = iterator.next();
	        if(converter instanceof MappingJackson2HttpMessageConverter){
	            iterator.remove();
	        }
	    }

		Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
		builder.serializationInclusion(JsonInclude.Include.NON_NULL);
		ObjectMapper objectMapper = builder.build();
		SimpleModule simpleModule = new SimpleModule();
		simpleModule.addSerializer(Enum.class, new EnumJsonSerializer());
		simpleModule.addSerializer(Long.class, new LongJsonSerializer());
		simpleModule.addSerializer(Date.class, new DateJsonSerializer());
		simpleModule.addDeserializer(Long.class, new LongJsonDeserializer());
		objectMapper.registerModule(simpleModule);
		objectMapper.configure(MapperFeature.PROPAGATE_TRANSIENT_MARKER, true);
		converters.add(new MappingJackson2HttpMessageConverter(objectMapper));
	}


//	public void addResourceHandlers(ResourceHandlerRegistry registry) {
//		registry.addResourceHandler("/**").addResourceLocations("/static");
//	}

}
*/
