package com.store.interceptor;

import com.store.beans.ApiResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class MyControllerAdvice {

	protected final static Logger logger = LoggerFactory.getLogger(MyControllerAdvice.class);

	@ResponseBody
	@ExceptionHandler(value = { Exception.class })
	public ApiResult<String> errorHandler(Exception ex) {

		if (ex instanceof BindException) {
			BindException bindException = (BindException) ex;

			return ApiResult.prepareError(bindException.getFieldError().getDefaultMessage());
		}
		logger.error("拦截错误消息", ex); 
		return ApiResult.prepareError("异常：" + ex.getMessage());
	}

}
