package com.store.id;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;

public class SnowflakeId implements IdentifierGenerator {
	
	public SnowflakeId() {}
	
	protected static final SnowflakeIdWorker idWorker = new SnowflakeIdWorker(1, 1);

	public Serializable generate(SessionImplementor session, Object object) throws HibernateException {

		return idWorker.nextId();
	}

	public static SnowflakeIdWorker getIdWorker(){
		return idWorker ;
	}
	public static void main(String[] args) {
		System.out.println(SnowflakeId.getIdWorker().nextId());
	}
}
