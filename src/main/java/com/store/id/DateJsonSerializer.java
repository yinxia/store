package com.store.id;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;


public class DateJsonSerializer extends JsonSerializer<Date> {
    @Override
    public void serialize(Date value, JsonGenerator gen, SerializerProvider serializers)throws IOException, JsonProcessingException {
        
    	if (null == value) {
			gen.writeString(StringUtils.EMPTY);
			return;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(value);
		if (calendar.get(Calendar.HOUR_OF_DAY) == 0 && calendar.get(Calendar.MINUTE) == 0) {
			String dateStr = DateFormatUtils.format(value, "yyyy-MM-dd");
			gen.writeString(dateStr);
		}else{
			String dateStr = DateFormatUtils.format(value, "yyyy-MM-dd HH:mm:ss");
			gen.writeString(dateStr);
		}
    }
}