package com.store.id;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.store.utils.EnumTools;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.IOException;


@SuppressWarnings({ "rawtypes" })
public class EnumJsonSerializer extends JsonSerializer<Enum> {

	@Override
	public void serialize(Enum value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		if (null == value) {
			return;
		}
		int code = NumberUtils.toInt(EnumTools.getInvokeValue(value, "getCode"));
		if (code == -1) {
			code = value.ordinal();
		}
		gen.writeString(String.valueOf(code));

	}

}
