package com.store.services;

import com.store.base.BaseServices;
import com.store.entity.OrderWares;
import com.store.repository.OrderWaresRepository;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @ClassName OrderWaresService
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/6 0006 10:32
 * @Version 1.0
 */

@Service
@Transactional
@Log4j
public class OrderWaresService extends BaseServices<OrderWares,Long> {

    protected OrderWaresRepository repository;

    public OrderWaresService(OrderWaresRepository repository) {
        super(repository);
        this.repository=repository;
    }

    public List<OrderWares> findByOrderId(Long orderId){
        return repository.findByOrderId(orderId);
    }


}
