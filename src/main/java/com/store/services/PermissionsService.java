package com.store.services;

import com.store.base.BaseServices;
import com.store.entity.Permissions;
import com.store.repository.PermissionsRepository;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @ClassName PermissionsService
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/16 0016 17:14
 * @Version 1.0
 */
@Service
public class PermissionsService extends BaseServices<Permissions,Long> {

    protected PermissionsRepository repository;

    public PermissionsService(PermissionsRepository repository) {
        super(repository);
        this.repository=repository;
    }

    public List<Permissions> findPermissionsNames(Long roleId){
        return repository.findPermissionsNames(roleId);
    }

}
