package com.store.services;

import com.store.base.BaseServices;
import com.store.entity.Preferential;
import com.store.repository.PreferentialRepository;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @ClassName PreferentialService
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/5 0005 10:36
 * @Version 1.0
 */
@Service
@Transactional
@Log4j
public class PreferentialService extends BaseServices<Preferential,Long> {


    protected PreferentialRepository repository;

    public PreferentialService(PreferentialRepository repository) {
        super(repository);
        this.repository=repository;
    }





}
