package com.store.services;

import com.store.base.BaseServices;
import com.store.entity.RoleOrMenu;
import com.store.repository.RoleOrMenuRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @ClassName RoleOrMenuService
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/29 0029 15:12
 * @Version 1.0
 */
@Service
@Transactional
public class RoleOrMenuService extends BaseServices<RoleOrMenu,Long> {

    protected RoleOrMenuRepository repository;

    public RoleOrMenuService(RoleOrMenuRepository repository) {
        super(repository);
        this.repository=repository;
    }

    public List<RoleOrMenu> findByRoleId(Long roleId){
        return repository.findByRoleId(roleId);
    }
}
