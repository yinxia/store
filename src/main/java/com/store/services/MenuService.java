package com.store.services;

import com.store.base.BaseServices;
import com.store.entity.Menu;
import com.store.repository.MenuRepository;
import org.springframework.stereotype.Service;

/**
 * @ClassName MenuService
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/29 0029 15:12
 * @Version 1.0
 */


@Service
public class MenuService extends BaseServices<Menu,Long> {

    protected MenuRepository repository;

    public MenuService(MenuRepository repository) {
        super(repository);
        this.repository=repository;
    }
}
