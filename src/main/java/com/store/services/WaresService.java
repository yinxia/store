package com.store.services;

import com.store.base.BaseServices;
import com.store.entity.InfoUploadFile;
import com.store.entity.Wares;
import com.store.pager.Condition;
import com.store.pager.Pager;
import com.store.pager.RestrictionType;
import com.store.repository.WaresRepository;
import com.store.utils.ObjectTools;
import com.store.utils.UploadUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @ClassName WaresService
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/5/27 0027 10:37
 * @Version 1.0
 */

@Service
@Transactional
public class WaresService extends BaseServices<Wares,Long> {

    protected WaresRepository repository;

    public WaresService(WaresRepository repository) {
        super(repository);
        this.repository=repository;
    }
    @Autowired
    private InfoUploadFileService fileService;


    public Wares saveOrUpdate(Wares wares) {

        Wares wares1 = null;

        Set<String> detailsFigureIds =new HashSet<>();
        if(wares.getId() != null && wares.getId() != 0){
            wares1 = this.findOne(wares.getId());
            detailsFigureIds = new HashSet<>(StringUtils.isEmpty(wares1.getDetailsFigureId())  ? new HashSet<>() : Arrays.asList(wares1.getDetailsFigureId() .split(",")));
        }else{
            super.saveOrUpdate(wares);
        }

        //上传附件s
        if(wares.getDetailsFigureFiles() != null && wares.getDetailsFigureFiles().size() != 0){
            List<InfoUploadFile> list = editInfo(detailsFigureIds, wares.getDetailsFigureFiles(),wares,wares.getProductName());
            wares.setDetailsFigureFiles(list);
            wares.setDetailsFigureId(StringUtils.join(list.stream().map(InfoUploadFile::getId).collect(Collectors.toList()), ","));
        }else{
            wares.setDetailsFigureFiles(new ArrayList<>());
            wares.setDetailsFigureId("");
        }

        if(null != wares1  && null != wares1.getCover()) {
            if (!wares.getCover().equals(wares1.getCover())) {
                deleteFile(wares1.getCover());
            }
        }

        if(null == wares.getSorts()){
            wares.setSorts("无");
            wares.setPutPullShelves("下架");
        }

        super.saveOrUpdate(wares);

        //删除被删除的文件
        if(detailsFigureIds.size() != 0){
            deleteFile2(detailsFigureIds);
        }

        return wares;
    }


    public  List<InfoUploadFile> editInfo (Set<String> set, List<InfoUploadFile> vo,Wares wares,String title){
        List<InfoUploadFile> fileList = new ArrayList<>();
        for(int i = 0 ; i < vo.size() ; i++){
            InfoUploadFile restInfoUploadFileVo = vo.get(i);
            if(restInfoUploadFileVo.getId() != null && restInfoUploadFileVo.getId() != 0){
                set.remove(restInfoUploadFileVo.getId()+"");
                InfoUploadFile file = fileService.findOne(restInfoUploadFileVo.getId());
                if(file != null ){
                    //file.setVal(restInfoUploadFileVo.getVal());
                    restInfoUploadFileVo = fileService.saveOrUpdate(file);
                }
            }else{
                MultipartFile file = restInfoUploadFileVo.getFile();
                try {
                    restInfoUploadFileVo = fileService.upload2(UploadUtils.convertFile(file),restInfoUploadFileVo.getVal(),"商品详情图/"+title,wares.getId());
                }catch (Exception e) {
                    System.out.println(e);
                    throw new RuntimeException("上传文件失败！");
                }
            }
            restInfoUploadFileVo.setFile(null);
            fileList.add(restInfoUploadFileVo);
        }
        //redisService.del("dirsTree");
        return fileList;
    }

    public void deleteFile(String photoFileId){

        Set<String> finalUploads = new HashSet<>(StringUtils.isEmpty(photoFileId)  ? new HashSet<>() : Arrays.asList(photoFileId.split(",")));

        Long [] ids = new Long[finalUploads.size()];
        for(int i = 0 ; i < finalUploads.size() ; i++){
            ids[i] = Long.parseLong(finalUploads.toArray()[i]+"");
        }

        fileService.deletes(ids);
    }


    public void deleteFile2( Set<String> set ){
        Long [] ids = new Long[set.size()];
        for(int i = 0 ; i < set.size() ; i++){
            ids[i] = Long.parseLong(set.toArray()[i]+"");
        }
        fileService.deletes(ids);
    }


    public Wares details(Long id){

        Wares wares = this.findOne(id);
        List<InfoUploadFile>  vo1 = new ArrayList<>();
        if(null != wares){
            Set<String> list1 = new HashSet<>(StringUtils.isEmpty(wares.getDetailsFigureId()) ? new HashSet<>() : Arrays.asList(wares.getDetailsFigureId().split(",")));
            if(list1.size() > 0){
                list1.stream().forEach(k ->{
                    InfoUploadFile restInfoUploadFileVo = fileService.findOne(ObjectTools.toLong(k));
                    if(restInfoUploadFileVo != null && restInfoUploadFileVo.getId() != null )  vo1.add(restInfoUploadFileVo);
                });
            }
            wares.setDetailsFigureFiles(vo1);
        }

        InfoUploadFile restInfoUploadFileVo = fileService.findOne(ObjectTools.toLong(wares.getCover()));
        if(restInfoUploadFileVo != null && restInfoUploadFileVo.getId() != null ){
            wares.setCovers(restInfoUploadFileVo);
        }

        return wares;
    }

    public Pager<Wares> getPagerAllList(Pager<Wares> pager, Wares wares){


        if(StringUtils.isNotBlank(wares.getClassification())){
            pager.addCondition(new Condition("classification",wares.getClassification(), RestrictionType.EQ));
        }

        if(StringUtils.isNotBlank(wares.getTaste())){
            pager.addCondition(new Condition("taste",wares.getTaste(), RestrictionType.EQ));
        }

        if(!ObjectTools.isNull(wares.getSpecifications())){
            pager.addCondition(new Condition("specifications",wares.getSpecifications(), RestrictionType.EQ));
        }

        if(!ObjectTools.isNull(wares.getPutPullShelves())){
            pager.addCondition(new Condition("putPullShelves",wares.getPutPullShelves(), RestrictionType.EQ));
        }

        if(StringUtils.isNotBlank(wares.getProductName())){
            pager.addCondition(new Condition("productName",wares.getProductName(), RestrictionType.LIKE));
        }

        pager.addOrder(new Pager.Order("sorts", Pager.OrderType.ASC));

        return super.findPager(pager);
    }



}
