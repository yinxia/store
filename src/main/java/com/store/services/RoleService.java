package com.store.services;


import com.store.base.BaseServices;
import com.store.entity.Role;
import com.store.entity.RoleOrMenu;
import com.store.repository.RoleRepository;
import com.store.utils.BeanTransform;
import com.store.utils.ObjectTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

/**
 * @ClassName RoleService
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/16 0016 17:08
 * @Version 1.0
 */

@Service
@Transactional
public class RoleService extends BaseServices<Role,Long> {

    protected RoleRepository repository;

    public RoleService(RoleRepository repository) {
        super(repository);
        this.repository=repository;
    }


    @Autowired
    private RoleOrMenuService roleOrMenuService;

    public List<Role> findRoles(Long userId){
        return repository.findRoles(userId);
    }


    public Role saveOrUpdate(String json){

        JsonParser jsonParser = JsonParserFactory.getJsonParser();
        Map<String, Object> map = jsonParser.parseMap(json);

        Role role  = BeanTransform.map2Entity(map,Role.class);
        this.saveOrUpdate(role);


        Set<Long> longs = new HashSet<>();
        if(!ObjectTools.isNull(map.get("roleOrMenuList"))) {
            List<Map<String, Object>> columns = (List<Map<String, Object>>) map.get("roleOrMenuList");

            columns.stream().forEach(v ->{
                RoleOrMenu roleOrMenu = BeanTransform.map2Entity(v,RoleOrMenu.class);
                roleOrMenu.setRoleId(role.getId());
                roleOrMenuService.saveOrUpdate(roleOrMenu);
                longs.add(roleOrMenu.getId());
            });

            //不存在的id就删除
            List<RoleOrMenu> roleOrMenus = roleOrMenuService.findByRoleId(ObjectTools.toLong(map.get("id")));
            roleOrMenus.stream().forEach(k ->{
                if(!longs.contains(k.getId())){
                    roleOrMenuService.delete(k.getId());
                }
            });
        }

        return role;
    }


    static final String R = "游客";

    public List<Map<String,Object>> findRoles(){
        List<Role> roles = this.findAll();
        List<Map<String,Object>> maps = new ArrayList<>();
        if(R.equals("游客")){
            roles.stream().forEach(k ->{
                Map<String,Object> map = new HashMap<>();
                map.put("role",k.getRole());
                map.put("userId",k.getUserId());
                maps.add(map);
            });
        }else {
            roles.stream().forEach(k ->{
                Map<String,Object> map = new HashMap<>();
                map.put("role",k.getRole());
                map.put("userId",k.getUserId());
                map.put("roleName","测试");
                maps.add(map);
            });
        }
        return maps;
    }


}
