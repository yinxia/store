package com.store.services;

import com.store.base.BaseServices;
import com.store.entity.InfoUploadFile;
import com.store.repository.InfoUploadFileRepository;
import com.store.utils.FileSizeUtils;
import lombok.extern.log4j.Log4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

/**
 * @ClassName InfoUploadFileService
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/5/27 0027 10:52
 * @Version 1.0
 */
@Service
@Transactional
@Log4j
public class InfoUploadFileService extends BaseServices<InfoUploadFile,Long> {


    @Value("${updateDir:}")
    private String updateDir;


    protected InfoUploadFileRepository repository;

    public InfoUploadFileService(InfoUploadFileRepository repository) {
        super(repository);
        this.repository=repository;
    }

    public void show(Long id, HttpServletResponse response) throws Exception {
        InfoUploadFile attachment = this.findOne(id);
        if (null != attachment) {
            String filePath = updateDir + "/" + attachment.getPath();
            File file = new File(filePath);
            if (!file.exists()) {
                response.sendError(404);
                return;
            }

            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Content-Length", String.valueOf(file.length()));
            response.setHeader("Content-Type", "image/png");
            org.apache.commons.io.IOUtils.copy(new FileInputStream(file), response.getOutputStream());
        }
    }

    public void deletes(Long [] ids) {
        Arrays.stream(ids).forEach(id ->{
            InfoUploadFile infoUploadFile = this.findOne(id);
            delFile(infoUploadFile.getPath());
            this.delete(id);
        });
    }


    public  void delFile(String path){
        File file = new File(path);
        if (file.exists()) {
            file.delete();
        }
    }


    /**
     * 处理文件上传(不存放在目录下)
     *
     * @param file
     * @return
     */
    public InfoUploadFile upload2(MultipartFile file, String val, String path,Long mainId) {
        InfoUploadFile infoUploadFile = new InfoUploadFile();
        if( file == null || StringUtils.isBlank(file.getOriginalFilename())){
            return infoUploadFile;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String orgFileName = file.getOriginalFilename();
        String fileName = UUID.randomUUID().toString() + "."
                + FilenameUtils.getExtension(orgFileName);
        //String path = "/sixdu/upload/"+ sdf.format(new Date());
        try {

            log.error("fileName " +fileName);
            String type = fileName.substring(fileName.lastIndexOf("."));
            log.error("type:  "+type.substring(1,type.length()));
            File targetFile = new File("/data/store/"+ path + sdf.format(new Date()), fileName);
            FileUtils.writeByteArrayToFile(targetFile, file.getBytes());

            infoUploadFile.setPath("/data/store/"+ path + sdf.format(new Date())+"/"+fileName);
            infoUploadFile.setVal(val);
            infoUploadFile.setSize(FileSizeUtils.convFileSize(file.getSize(),"K"));
            infoUploadFile.setName(file.getOriginalFilename());
            infoUploadFile.setType(type.substring(1,type.length()));
            infoUploadFile.setInfoId(mainId);
            this.saveOrUpdate(infoUploadFile);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return infoUploadFile;
    }


}
