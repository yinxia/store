package com.store.services;

import com.store.base.BaseServices;
import com.store.entity.InfoUploadFile;
import com.store.entity.StoreRoundImg;
import com.store.pager.Pager;
import com.store.repository.StoreRoundImgRepository;
import com.store.utils.ObjectTools;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;

/**
 * @ClassName StoreRoundImgService
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/5/27 0027 11:02
 * @Version 1.0
 */
@Service
@Transactional
@Log4j
public class StoreRoundImgService extends BaseServices<StoreRoundImg,Long> {

    protected StoreRoundImgRepository repository;

    public StoreRoundImgService(StoreRoundImgRepository repository) {
        super(repository);
        this.repository=repository;
    }

    @Autowired
    private InfoUploadFileService fileService;

    public StoreRoundImg saveOrUpdate(MultipartFile file, StoreRoundImg storeRoundImg){
        if (null != file) {
            InfoUploadFile fileVo = fileService.upload2(file, "","轮播图", 0L);
            if (!ObjectTools.isNull(storeRoundImg.getImgId())){
                Long[] ids={storeRoundImg.getImgId()};
                fileService.deletes(ids);
            }
            storeRoundImg.setImgId(fileVo.getId());

        }
        this.saveOrUpdate(storeRoundImg);
        return storeRoundImg;
    }


    public Pager<StoreRoundImg> findPage(Pager<StoreRoundImg> pager) {
        pager.addOrder(new Pager.Order("updateDate", Pager.OrderType.DESC));
        pager = super.findPager(pager);
        return pager;
    }

    public StoreRoundImg details(Long id){
        StoreRoundImg storeRoundImg = this.findOne(id);
        if(null != storeRoundImg) {
            InfoUploadFile infoUploadFile = fileService.findOne(storeRoundImg.getImgId());
            storeRoundImg.setInfoUploadFile(infoUploadFile);
        }
        return storeRoundImg;
    }



}
