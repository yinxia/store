package com.store.services;

import com.store.base.BaseServices;
import com.store.entity.StoreUser;
import com.store.repository.StoreUserRepository;
import com.store.utils.MD5;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;

/**
 * @ClassName: StoreUserService
 * @Author: pengyongliang
 * @Date: 19-6-27 下午2:50
 * @Desription:
 **/
@Service
@Transactional
public class StoreUserService extends BaseServices<StoreUser,Long>{

    protected StoreUserRepository repository;


    public StoreUserService(StoreUserRepository repository) {
        super(repository);
        this.repository=repository;
    }


    public StoreUser findByphone(String phone){
        return repository.findByPhone(phone);
    }


    public StoreUser register(StoreUser user) {

        user.setUName(user.getPhone());
        //String code = RandomUtils.verifyUserName(2, 8);
        user.setPassword(MD5.md5(user.getPassword()));
        user.setPhone(user.getPhone());
        user.setUpdateDate(new Date());
        user.setCreateDate(new Date());
        repository.save(user);

        return user;
    }



    public int updateLoginTime(Date time, String phone) {
        return repository.updateLoginTime(time, phone);
    }



}
