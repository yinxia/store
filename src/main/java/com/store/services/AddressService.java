package com.store.services;

import com.store.base.BaseServices;
import com.store.entity.Address;
import com.store.repository.AddressRepository;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @ClassName AddressService
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/5 0005 11:05
 * @Version 1.0
 */

@Service
@Transactional
@Log4j
public class AddressService extends BaseServices<Address,Long> {

    protected AddressRepository repository;

    public AddressService(AddressRepository repository) {
        super(repository);
        this.repository=repository;
    }


}
