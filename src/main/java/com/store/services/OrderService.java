package com.store.services;

import com.store.base.BaseServices;
import com.store.entity.Address;
import com.store.entity.Order;
import com.store.entity.OrderWares;
import com.store.entity.Wares;
import com.store.pager.Condition;
import com.store.pager.Pager;
import com.store.pager.RestrictionType;
import com.store.repository.OrderRepository;
import com.store.utils.ObjectTools;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @ClassName OrderService
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/6 0006 10:26
 * @Version 1.0
 */

@Service
@Transactional
@Log4j
public class OrderService extends BaseServices<Order,Long> {

    protected OrderRepository repository;

    public OrderService(OrderRepository repository) {
        super(repository);
        this.repository=repository;
    }

    @Autowired
    private OrderWaresService orderWaresService;

    @Autowired
    private AddressService addressService;

    @Autowired
    private WaresService waresService;


    public Order saveOrUpdate(String [] strs, Order order){

        order.setHaveRead("2");
        this.saveOrUpdate(order);

         if(null != strs && strs.length > 0) {
             Arrays.asList(strs).stream().forEach(k ->{
                 OrderWares orderWares = new OrderWares();
                 orderWares.setWaresId(ObjectTools.toLong(k));
                 orderWares.setOrderId(order.getId());
                 orderWaresService.save(orderWares);
             });
         }

        return order;
    }

    public Order details(Long id){

        Order order = this.findOne(id);
        List<OrderWares> orderWaresLists = orderWaresService.findByOrderId(id);
        if(null != orderWaresLists){
            order.setOrderWaresLists(orderWaresLists);
        }
        return order;
    }




    public void del(Long id){

        List<OrderWares> orderWares = orderWaresService.findByOrderId(id);
        orderWares.stream().forEach(k ->{
            orderWaresService.delete(k.getId());
        });
        this.delete(id);
    }


    public Pager<Order> getPagerAllList(Pager<Order> pager, Order order, Date startTime,Date endTime,Date sendStartTime,Date sendEndTime){

        if(StringUtils.isNotBlank(order.getCourierNumber())){
            pager.addCondition(new Condition("courierNumber",order.getCourierNumber(), RestrictionType.LIKE));
        }

        if(!ObjectTools.isNull(order.getId())){
            pager.addCondition(new Condition("id",order.getId(), RestrictionType.EQ));
        }

        if(!ObjectTools.isNull(startTime)){
            pager.addCondition(new Condition("createDate",startTime, RestrictionType.GTEQ));
        }

        if(!ObjectTools.isNull(endTime)){
            pager.addCondition(new Condition("createDate",endTime, RestrictionType.LTEQ));
        }

        if(StringUtils.isNotBlank(order.getFullReduction())){
            pager.addCondition(new Condition("fullReduction",order.getFullReduction(), RestrictionType.EQ));
        }

        if(!ObjectTools.isNull(sendStartTime)){
            pager.addCondition(new Condition("sendTime",sendStartTime, RestrictionType.GTEQ));
        }

        if(!ObjectTools.isNull(sendEndTime)){
            pager.addCondition(new Condition("sendTime",sendEndTime, RestrictionType.LTEQ));
        }
        super.findPager(pager);
        pager.getContent().stream().forEach(k ->{
            List<OrderWares> orderWaresList = orderWaresService.findByOrderId(k.getId());
            orderWaresList.stream().forEach(v ->{
                Wares wares = waresService.findOne(v.getWaresId());
                v.setProductName(wares.getProductName() == null ? "" : wares.getProductName());
                v.setTaste(wares.getTaste()== null ? "" : wares.getTaste());
            });
            k.setOrderWaresLists(orderWaresList);
            if(!ObjectTools.isNull(k.getAddressId())) {
                Address address = addressService.findOne(k.getAddressId());
                k.setConsignee(address.getConsignee());
                k.setPhone(address.getPhone());
                k.setDetailedAddress(address.getDetailedAddress());
            }
            if(null != k.getFullReduction()) {
                if (k.getFullReduction().equals("1")) {
                    k.setFullReductionName("已支付");
                }
                if (k.getFullReduction().equals("2")) {
                    k.setFullReductionName("待支付");
                }
                if (k.getFullReduction().equals("3")) {
                    k.setFullReductionName("已完成");
                }
            }
        });
        return pager;
    }


}
