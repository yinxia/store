package com.store.vo;

import lombok.Data;

/**
 * @author pengyongliang
 * @desc
 * @date 2019/9/1
 */
@Data
public class Tweets {

    private String thumb_media_id;

    private String author;

    private String title;

    private String content_source_url;

    private String content;

    private String digest;

    private int show_cover_pic;

    private int only_fans_can_comment=1;

    private int need_open_comment=1;


}
