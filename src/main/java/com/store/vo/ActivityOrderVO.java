package com.store.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import javax.persistence.Transient;

/**
 * @ClassName: ActivityOrderVO
 * @Author: 陈天频
 * @Date: 19-7-11 下午5:47
 * @Desription:
 * @Copy: com.bjike
 **/
@Data
public class ActivityOrderVO {

    private Long id;

    private String createDate;

    private String orderNumber; //订单号

    //报名路径机构ID
    @JsonSerialize(using = ToStringSerializer.class)
    private Long signUpPath;

    //购买的活动id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long activityId;

    //孩子名称
    private String childrenName;
    //孩子年龄
    private int childrenAge;

    //购买手机号
    @JsonSerialize(using = ToStringSerializer.class)
    private Long phone;

    //活动名称
    private String activityName;

    //主办方
    private String sponsor;

    //活动地点
    private String activityAddress;

    //活动费用
    private String activityCost;

    //活动开始结束时间
    private String sdate;
    private String edate;

    //订单状态（0：未支付，1：已支付）
    private int orderState;

    //0：女1：男
    private int childrenSex;

    private String classNames;

    private String address;

    //父母是否陪同（0：否；1：是）
    private int accompany;

    //支付方式
    private String payment;

    private  String speciality;

    private String slogan;

    private String guardian;

    private String birthDate;

    private String experience;

    private String content ;
}
