package com.store.vo;

import lombok.Data;

/**
 * @author pengyongliang
 * @desc
 * @date 2019/9/1
 */
@Data
public class Material {

    private String type;

    private Integer offset;

    private Integer count;
}
