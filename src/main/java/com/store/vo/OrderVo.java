package com.store.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author pengyongliang
 * @desc
 * @date 2020/4/2
 */
@Data
public class OrderVo {

    protected Date createDate = new Date();

    private String orderNumber; //订单号

    private String tradeNumber;//交易号

    private Integer payType;//1线下2微信3支付宝

    private Integer totalMoney;//总金额

    private Long classCourseId;//班级课程id

    private Long studentId;//学生id

    private Date courseDate;//首次上课日期

    private Integer payPeriods;//购买总课节

    private Integer userPeriods;//使用了多少节课程

    private Integer sonPeriods;//子订单总课节

    private Integer sonUsePeriods;//子订单使用了多少节课程

    private Date payTime;//付款时间

    protected Double termOfValidity;//有效期

    private Integer status;//订单状态0未支付 1已支付 2已取消 3 体验课 4已停课 5已退课

    private Integer sonStatus;//子订单状态 0正常 1已停课

    private Long parentId;  //父订单id

    private Integer continueRenew;//是否继续续费 1不续费
}
