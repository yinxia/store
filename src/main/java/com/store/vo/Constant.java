package com.store.vo;

/**
 * @author pengyongliang
 * @desc
 * @date 2019/9/4
 */
public class Constant {

    //=======【基本信息设置】=====================================
    //微信公众号身份的唯一标识。
    //public static final String APPID = "wxe12b99f48476d602";//需改
    //JSAPI接口中获取openid，审核后在公众平台开启开发模式后可查看
    //public static final String APPSECRET = "7c43467a11442b1d12f7520a95e30741";//需改
    private static String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
    public static String getAccess_token_url(String appid,String appsecret){
        return ACCESS_TOKEN_URL.replace("APPID",appid).replace("APPSECRET",appsecret);
    }
    public static String templateMsgUrl= "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
}
