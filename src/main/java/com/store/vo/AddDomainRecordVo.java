package com.store.vo;

import lombok.Data;

@Data
public class AddDomainRecordVo {
    private String webAddress;
    private String backAddress;
    private String loginName;
    private String passWord;
}
