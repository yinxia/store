package com.store.convers;

import com.store.utils.ObjectTools;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component
public class DateConverterConfig implements Converter<String, Date> {
	
	private static final List<String> formarts = new ArrayList<>(4);
	
	private static DateConverterConfig config = new DateConverterConfig();
	
	static {
		formarts.add("yyyy-MM");
		formarts.add("yyyy-MM-dd");
		formarts.add("yyyy-MM-dd HH:mm");
		formarts.add("yyyy-MM-dd HH:mm:ss"); 
	}
	
	public static Date parseDate(String source) {
		return config.convert(source) ;
	}
	
	@Override
	public Date convert(String source) {
		
		String value = ObjectTools.toString(source);
		 value =  value.replaceAll("  ", " ");
		if ("".equals(value)) {
			return null;
		}
		if (value.matches("^\\d{4}-\\d{1,2}$")) {
			return parseDate(value, formarts.get(0));
		} else if (value.matches("^\\d{4}-\\d{1,2}-\\d{1,2}$")) {
			return parseDate(value, formarts.get(1));
		} else if (value.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}$")) {
			return parseDate(value, formarts.get(2));
		} else if (value.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}:\\d{1,2}$")) {
			return parseDate(value, formarts.get(3));
		} else {
			throw new IllegalArgumentException("Invalid date value '" + value + "'");
		}
	}

	public Date parseDate(String dateStr, String format) {
		try {
			return DateUtils.parseDate(dateStr, format);
		} catch (ParseException e) {
			throw new RuntimeException(e); 
		}
	}
}
