package com.store.pager;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

@JsonIgnoreProperties({"conditions" , "orders"})
@Data
@EqualsAndHashCode( callSuper =  false)
@JsonInclude( content = JsonInclude.Include.NON_NULL)
@SuppressWarnings("hiding")
public class Pager<T> implements Serializable {

    public enum OrderType{
        ASC(" ASC") , DESC(" DESC") ;

        OrderType(String sql) {
            this.sql = sql;
        }

        private String sql ;

        public String getSql() {
            return sql;
        }
    }

    @Data
    public static class Order{

        private String name ;

        private OrderType orderType ;

        public Order(String name, OrderType orderType) {
            this.name = name;
            this.orderType = orderType;
        }
    }

	private static final long serialVersionUID = 7471577058505212976L;

	private List<Condition> conditions = new ArrayList<>() ;

    private int page = 1 ;

    private int pageSize = 10;

    private long totalElements ;

    private long totalPages ;

    private int size ;

    private boolean first ;

    private boolean last ;

    private int numberOfElements ;

    private List<T> content = new Vector<>();

    private List<Order> orders = new Vector<>() ;

    public void addCondition(Condition condition){
        conditions.add( condition );
    }

    public void addOrder(Order order){
        orders.add( order ) ;
    }

	public <T> Pager<T> copy(){
        Pager<T> t = new Pager<T>();
        t.setSize( this.getSize() );
        t.setTotalElements( this.getTotalElements() );
        t.setFirst( this.isFirst() );
        t.conditions = this.getConditions() ;
        t.setLast( this.isLast() );
        t.setPage( this.getPage() );
        t.setPageSize( this.getPageSize() );
        t.setTotalPages( this.getTotalPages());
        t.setNumberOfElements( this.getNumberOfElements() );
        t.orders = this.getOrders() ;
        return t ;
    }

}
