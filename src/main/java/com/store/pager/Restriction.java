package com.store.pager;

/**
 * @ClassName: Restriction
 * @Author: 黄飞
 * @Date: 2018/7/3 9:27
 * @Desription: ${}
 * @Copy: ${com.bjike}
 **/
public class Restriction {

    public static Condition eq(String property , Object value){
        return new Condition( property , value , RestrictionType.EQ ) ;
    }

    public static Condition like(String property , Object value){
        return new Condition( property , value , RestrictionType.LIKE ) ;
    }

    public static Condition in(String property , Object value){
        return new Condition( property , value , RestrictionType.IN ) ;
    }

    public static Condition gt(String property , Object value){
        return new Condition( property , value , RestrictionType.GT ) ;
    }

    public static Condition gteq(String property , Object value){
        return new Condition( property , value , RestrictionType.GTEQ ) ;
    }

    public static Condition lt(String property , Object value){
        return new Condition( property , value , RestrictionType.LT ) ;
    }

    public static Condition lteq(String property , Object value){
        return new Condition( property , value , RestrictionType.LTEQ ) ;
    }
}
