package com.store.pager;

import java.io.Serializable;

public class Condition implements Serializable {
   
	private static final long serialVersionUID = 3726647076920014670L;
	/**
     * field（字段） 包含 "." 则默认会设置成左连接，左连接set 集合 命名必须未Set结束
     * 如：Set<User>userSet List<User>userList
     */
    private String field ;
    private Object value ;
    private RestrictionType restrict ;

    public Condition(String field, Object value, RestrictionType restrict) {
        this.field = field;
        this.value = value;
        this.restrict = restrict;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public RestrictionType getRestrict() {
        return restrict;
    }

    public void setRestrict(RestrictionType restrict) {
        this.restrict = restrict;
    }
}