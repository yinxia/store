package com.store.pager;

public enum RestrictionType {
    /**
     * 相等
     */
    EQ("="),

    /**
     * 模糊
     */
    LIKE("like"),
    /**
     * 在什么范围之间
     */
    IN("in"),

    /**
     * 大于
     */
    GT(">"),
    /**
     * 小于
     */
    LT("<"),
    /**
     * 或者
     */
    OR("or"),
    /**
     * 不等于
     */
    NE ("!="),
    /**
     * 大于等于
     */
    GTEQ(">="),
    /**
     * 小于等于
     */
    LTEQ("<=");
    private String symbol ;
    RestrictionType(String symbol){
        this.symbol = symbol ;
    }

    public String getSymbol() {
        return symbol;
    }
}