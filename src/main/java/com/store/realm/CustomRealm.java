package com.store.realm;

import com.store.entity.Permissions;
import com.store.entity.Role;
import com.store.entity.StoreUser;
import com.store.services.PermissionsService;
import com.store.services.RoleService;
import com.store.services.StoreUserService;
import com.store.utils.ObjectTools;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @ClassName CustomRealm
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/16 0016 17:02
 * @Version 1.0
 */

public class CustomRealm extends AuthorizingRealm {

    @Autowired
    private RoleService roleService;

    @Autowired
    private StoreUserService storeUserService;

    @Autowired
    private PermissionsService permissionsService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection Principal) {
        String username = (String) Principal.getPrimaryPrincipal();
        StoreUser storeUser = storeUserService.findByphone(username);
        if(null == storeUser){
            return null;
        }
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        //这里可以进行授权和处理
        Set<String> rolesSet = new HashSet<>();
        Set<String> permsSet = new HashSet<>();

        List<Role> roleList = roleService.findRoles(storeUser.getId());
        //Set<String> roles = roleService.findRoles(storeUser.getId());
        roleList.stream().forEach(k ->{
            rolesSet.add(k.getId() + "");
            List<Permissions> permissionsList = permissionsService.findPermissionsNames(k.getId());
            permissionsList.stream().forEach(v ->{
                permsSet.add(v.getPermissionsName());
            });
        });
        //将查到的权限和角色分别传入authorizationInfo中
        authorizationInfo.setStringPermissions(permsSet);
        authorizationInfo.setRoles(rolesSet);
        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        // 1.从主体传过来的认证信息中，获得用户名
        String username = (String) token.getPrincipal();
        // 2.通过用户名到数据库中获取凭证
        StoreUser storeUser = storeUserService.findByphone(username);
        if(null != storeUser){
            String password = storeUser.getPassword();
            SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(username,
                    password, this.getName());
            simpleAuthenticationInfo.setCredentialsSalt(ByteSource.Util.bytes(username));
            return simpleAuthenticationInfo;
        }
        return null;
    }

    public static void main(String[] args) {
        Md5Hash md5Hash = new Md5Hash("123456", "admin");
        System.out.println(md5Hash);
    }




}
