package com.store.utils;

import java.io.Serializable;
import java.util.TimeZone;

public interface Constant extends Serializable {

	String BIZ_TABLE_NAME_CONTRACT = "biz_contract";
	

	int DYNAMIC_CONTRACT_TABLE_SIZE = 4;

	String YYYYMMDD = "yyyyMMdd";
	String YYYYMM = "yyyyMM";
	String YYYY_MM_DD = "yyyy-MM-dd";
	String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
	String HH_MM_SS = "HH:mm:ss";

	String BIZ_DIS_NAME_TITLE = "派工条目";

	String BIZ_NUM_TITLE = "规模数量";

	String BIZ_UNIT_TITLE = "单位";

	String BIZ_PRICE_TITLE = "单价";
	 
	String YYYYMMDDHH = "yyyyMMddHH" ;
	
	String HEADER_AUTHORIZATION = "Authorization" ;
	
	String GMT8 = "GMT+8" ;
	
	TimeZone TIME_ZONE = TimeZone.getTimeZone("GMT+8") ;
	
	String BET_BIZ = "biz" ;
	
	String PYTHON_URL = "http://39.106.15.247:8000" ;
}
