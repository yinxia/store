package com.store.utils;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.store.beans.ApiResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: SmsService
 * @Author: 黄飞
 * @Date: 2018/6/25 11:16
 * @Desription: ${}
 * @Copy: ${com.bjike}
 **/
@Service
public class SmsService {

    public static final String VERIFICATION_TMP_ID = "SMS_100300045" ;

    final String product = "Dysmsapi";//短信API产品名称（短信产品名固定，无需修改）
    final String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
    //替换成你的AK
    final String accessKeyId = "LTAIIiJILKkH0JGY";//你的accessKeyId,参考本文档步骤2
    final String accessKeySecret = "KmmxKgS8f3MVIdAEQ3NTv2Qzouetu1";//你的accessKeySecret，参考本文档步骤2

    public ApiResult<String> send(String phone , String templateCode , String code){
        Map<String,String> codeMap = new HashMap<>() ;
        codeMap.put("code" , code) ;
        return send(phone , templateCode , codeMap ) ;
    }


    public ApiResult<String> send(String phone , String templateCode , Map<String,String> code){
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId,accessKeySecret);
        try{
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            SendSmsRequest request = new SendSmsRequest();
            request.setMethod(MethodType.POST);
            request.setPhoneNumbers( phone );
            request.setSignName("艾佳");
            request.setTemplateCode( templateCode );
            ObjectMapper objectMapper = new ObjectMapper();
            request.setTemplateParam( objectMapper.writeValueAsString( code ) );
            request.setOutId( "001" );
            SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
            if(StringUtils.equalsAny("OK" , sendSmsResponse.getCode() )) {
                return ApiResult.prepareString(  sendSmsResponse.getMessage() ) ;
            }else{
                return ApiResult.prepareError( sendSmsResponse.getMessage() ) ;
            }
        }catch (Exception e){
            return ApiResult.prepareError("发送短信出现错误：" + e.getMessage() );
        }
    }


}
