package com.store.utils;

import org.apache.commons.io.IOUtils;

import java.io.*;

/**
 * @ClassName: ObjectSerializeUtils
 * @Author: 黄飞
 * @Date: 2018/6/22 9:12
 * @Desription: ${}
 * @Copy: ${com.bjike}
 **/
public class ObjectSerializeUtils {

    protected static final byte STRING = 1 ;
    protected static final byte OBJECT = 2 ;

    protected static final byte[] EMPTY_BYTE = new byte[]{} ;

    public static Object toObject(byte[] bytes){
        ByteArrayInputStream bis = null ;
        ObjectInputStream ois = null ;
        try{

            bis = new ByteArrayInputStream(bytes ) ;
            ois = new ObjectInputStream(bis);
            return ois.readObject() ;
        }catch (Exception e){
            e.printStackTrace();
            return null ;
        }finally {
            IOUtils.closeQuietly( bis );
            IOUtils.closeQuietly( ois );
        }
    }

    public static byte[] toByte(Object ob){

        if(null == ob){
            return EMPTY_BYTE ;
        }

        if(ob instanceof String){
            byte[]bytes = ob.toString().getBytes() ;

            return bytes ;
        }

        if(!(ob instanceof Serializable)){
            return EMPTY_BYTE ;
        }
        ByteArrayOutputStream bos = null ;
        ObjectOutputStream oos = null ;
        try{
           bos = new ByteArrayOutputStream();
           oos = new ObjectOutputStream(bos) ;

            oos.writeObject( ob );

            byte[] bytes =  bos.toByteArray() ;

            return bytes ;
       }catch(Exception e){
            return EMPTY_BYTE ;
       }finally {
            IOUtils.closeQuietly( oos );
            IOUtils.closeQuietly( bos );
        }
    }

    public static void main(String[]args){
        byte[]b = toByte( 1 ) ;

        System.out.println( b.length );
        System.out.println(toObject(b));
    }
}
