package com.store.utils;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Sort;
import org.springframework.data.geo.Circle;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;


@Service
@ConditionalOnProperty("spring.redis.host")
public class RedisService {


	public RedisTemplate redisTemplate ;


	protected static final byte[] STATIC_BYTE = new byte[]{};

	/*@Autowired
	public RedisService(RedisTemplate redisTemplate){
		this.redisTemplate = redisTemplate ;

		RedisSerializer<String> keySerializer = new RedisSerializer<String>() {

			@Override
			public String deserialize(byte[] bytes) throws SerializationException {
				return ObjectUtils.isEmpty(bytes) ? StringUtils.EMPTY : new String(bytes);
			}

			@Override
			public byte[] serialize(String t) throws SerializationException {
				return null == t ? STATIC_BYTE : t.getBytes(  ) ;
			}
		} ;

		this.redisTemplate.setKeySerializer( keySerializer );
		this.redisTemplate.setHashKeySerializer( keySerializer );
		this.redisTemplate.setDefaultSerializer( keySerializer );

	}*/

	public void appendList(String key,Long value){
		redisTemplate.opsForZSet().add(key , String.valueOf( value) , value.longValue() ) ;
	}

	public List<Long> range(String key , Long value){
		Set<Object> set = redisTemplate.opsForZSet().range(key , value  , Long.MAX_VALUE) ;
		List<Long> list = new ArrayList<>();

		set.stream().collect(()-> new ArrayList<Long>(),(conn,v)->{
			list.add( NumberUtils.toLong(v == null ? "0" : v+"" ) );
		},(list1,list2)->{
			list1.addAll(list2) ;
		});

		return list ;
	}

	public void set(String key , Object value , long time){
		redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS );
	}

	public void set(String key , Object value ){
		set(key, value, 30 * 60 );
	}

	public Object get(String key){
		return redisTemplate.opsForValue().get( key ) ;
	}

	public boolean hasKey(String key) {
	 	return redisTemplate.hasKey(key);
	}

	public Set keys(Object key) {
		return redisTemplate.keys(key);
	}

	public void del(Object key) {
		 redisTemplate.delete(key);
	}

	public boolean expire(Object key, long timeout, TimeUnit timeUnit){return redisTemplate.expire(key,timeout,timeUnit);}

	public void geoadd(String key,double lg , double lt,Object member){
		redisTemplate.opsForGeo().geoAdd(key,new Point(lg,lt),member);
	}
	public void geodel(String key,Object member){
		redisTemplate.opsForGeo().geoRemove(key,member);
	}
	public void geoDistance(String key,Object member1,Object member2){
		redisTemplate.opsForGeo().geoDist(key,member1,member2);
	}
	/**
	 *
	 * @MethodName：radiusGeo
	 * @param distance km
	 * @return
	 * @Description：通过给定的坐标和距离(km)获取范围类其它的坐标信息
	 */
	public GeoResults<RedisGeoCommands.GeoLocation<String>> geoRadius(String key, double lg , double lt, double distance , String direction , long limit){
		RedisGeoCommands.GeoRadiusCommandArgs geoRadiusArgs = RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs();
		geoRadiusArgs = geoRadiusArgs.includeCoordinates().includeDistance();//查询返回结果包括距离和坐标
		if (Sort.Direction.ASC.equals(direction)) {//按查询出的坐标距离中心坐标的距离进行排序
			geoRadiusArgs.sortAscending();
		} else if (Sort.Direction.DESC.equals(direction)) {
			geoRadiusArgs.sortDescending();
		}
		geoRadiusArgs.limit(limit);//限制查询数量
		GeoResults<RedisGeoCommands.GeoLocation<String>> radiusGeo = redisTemplate.opsForGeo().geoRadius(key,
				new Circle(new Point(lg, lt), new Distance(distance, RedisGeoCommands.DistanceUnit.METERS)),
				geoRadiusArgs);
		return radiusGeo;
	}

}
