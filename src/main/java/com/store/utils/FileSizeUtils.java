package com.store.utils;

public class FileSizeUtils {
    /**
     * 判断文件大小
     *
     * @param len
     *            文件长度
     * @param size
     *            限制大小
     * @param unit
     *            限制单位（B,K,M,G）
     * @return
     */
    public static boolean checkFileSize(Long len, int size, String unit) {
//        long len = file.length();
        double fileSize = 0;
        if ("B".equals(unit.toUpperCase())) {
            fileSize = (double) len;
        } else if ("K".equals(unit.toUpperCase())) {
            fileSize = (double) len / 1024;
        } else if ("M".equals(unit.toUpperCase())) {
            fileSize = (double) len / 1048576;
        } else if ("G".equals(unit.toUpperCase())) {
            fileSize = (double) len / 1073741824;
        }
        if (fileSize > size) {
            return false;
        }
        return true;
    }


    /**
     * 转换文件大小
     *
     * @param len
     *            文件长度
     * @param size
     *            限制大小
     * @param unit
     *            限制单位（B,K,M,G）
     * @return
     */
    public static double convFileSize(Long len, String unit) {
//        long len = file.length();
        if ("B".equals(unit.toUpperCase())) {
            return  (double) len;
        } else if ("K".equals(unit.toUpperCase())) {
            return (double) len / 1024;
        } else if ("M".equals(unit.toUpperCase())) {
            return (double) len / 1048576;
        } else if ("G".equals(unit.toUpperCase())) {
            return  (double) len / 1073741824;
        }
       return (double)len;
    }
}
