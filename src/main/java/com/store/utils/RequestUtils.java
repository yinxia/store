package com.store.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName: RequestUtils
 * @Author: 陈天频
 * @Date: 19-7-3 上午11:05
 * @Desription:
 * @Copy: com.bjike
 **/
public class RequestUtils {

    public static String FindWebAddress(HttpServletRequest request){
       // StringBuffer url = request.getRequestURL();
        //http://127.0.0.1:8092/api/user/login
        /*String webAddress = url.delete(url.length() - request.getRequestURI().length(), url.length())
                .append(request.getSession().getServletContext().getContextPath()).append("/").toString();*/
        String webAddress=request.getHeader("Origin");
        return webAddress;
    }
}
