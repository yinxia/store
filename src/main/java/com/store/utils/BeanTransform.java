package com.store.utils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.store.exceptions.ServiceRuntimeException;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class BeanTransform {


    public static <TARGET, SOURCE> TARGET copyProperties(SOURCE source, Class<TARGET> target) {
        if (null != source) {
            try {
                TARGET o_target = target.newInstance();
                BeanInfo info = Introspector.getBeanInfo(source.getClass());
                for (PropertyDescriptor property : info.getPropertyDescriptors()) {
                    Field sourceField = FieldUtils.getField(source.getClass(), property.getName(), true);
                    if (null == sourceField) {
                        continue;
                    }
                    if (Modifier.isStatic(sourceField.getModifiers()) || Modifier.isFinal(sourceField.getModifiers())) {
                        continue;
                    }
                    try {
                        Object value = FieldUtils.readDeclaredField(source, property.getName(), true);
                        Field targetField = FieldUtils.getField(target, property.getName(), true);
                        if (null != targetField) {
                            FieldUtils.writeDeclaredField(o_target, property.getName(), value, true);
                        }
                    } catch (Exception e) {
                    }

                }
                return o_target;
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
            }
        }
        return null;
    }

    public static Map<String, Object> entityToMap(Object source) {
        Map<String, Object> result = new HashMap<>() ;
        try {
            BeanInfo info = Introspector.getBeanInfo(source.getClass());
            for (PropertyDescriptor property : info.getPropertyDescriptors()) {
                Field sourceField = FieldUtils.getField(source.getClass(), property.getName(), true);
                if (null == sourceField) {
                    continue;
                }
                if (Modifier.isStatic(sourceField.getModifiers()) || Modifier.isFinal(sourceField.getModifiers())) {
                    continue;
                }
                sourceField.setAccessible( true );
                Object value = sourceField.get( source );
                if(null == value){
                    continue;
                }
                if(value instanceof Date){
                    JsonFormat format = sourceField.getAnnotation(JsonFormat.class) ;
                    String pattern = "yyyy-MM-dd" ;
                    if(null != format){
                        pattern = format.pattern() ;
                    }
                    value = DateFormatUtils.format((Date)value , pattern );
                }else if(value instanceof Long){
                    value = StringTools.toString( value ) ;
                }
                result.put(sourceField.getName() , value) ;
            }
        } catch (Exception e) {
            throw new ServiceRuntimeException( e  ) ;

        }
        return result;
    }



    public static <TARGET> TARGET map2Entity(Map<String, Object> source, Class<TARGET> target) {

        try {
            final TARGET res = target.newInstance();
            source.forEach((key, value) -> {

                if (null == value || "".equalsIgnoreCase(String.valueOf(value))) {
                    return;
                }

                Field field = getField(target, key);
                if (null != field) {
                    try {
                        Class<?> filedType = field.getType();
                        field.setAccessible(true);
                        if (Integer.class.equals(filedType) || int.class.equals(filedType)) {
                            field.set(res, NumberUtils.toInt(StringTools.toString(value)));
                        } else if (Double.class.equals(filedType) || double.class.equals(filedType)) {
                            field.set(res, NumberUtils.toDouble(StringTools.toString(value)));
                        } else if (Long.class.equals(filedType) || long.class.equals(filedType)) {
                            field.set(res, NumberUtils.toLong(StringTools.toString(value)));
                        } else if (Float.class.equals(filedType) || float.class.equals(filedType)) {
                            field.set(res, NumberUtils.toFloat(StringTools.toString(value)));
                        } else if (Byte.class.equals(filedType) || byte.class.equals(filedType)) {
                            field.set(res, NumberUtils.toByte(StringTools.toString(value)));
                        } else if (Date.class.equals(filedType) || java.sql.Date.class.equals(filedType)
                                || Timestamp.class.equals(filedType)) {
                            Date date = DateUtils.parseDate(String.valueOf(value), "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss");
                            field.set(res, new Timestamp(date.getTime()));
                        } else {
                            field.set(res, value);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            return res;
        } catch (Exception e) {
            throw new ServiceRuntimeException("map2Entity :" + e.getMessage());
        }
    }

    protected static Field getField(Class<?> target, String name) {
        Class<?> thisClazz = target;
        while (!Object.class.equals(thisClazz)) {
            Field field = FieldUtils.getField(thisClazz, name, true);
            if (null == field) {
                thisClazz = thisClazz.getSuperclass();
                break;
            }
            return field;
        }
        return null;
    }

}
