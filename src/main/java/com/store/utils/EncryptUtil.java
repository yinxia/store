package com.store.utils;

import org.apache.commons.lang3.StringUtils;

import java.security.MessageDigest;
import java.util.Map;
import java.util.TreeMap;

public class EncryptUtil {
	
	public static final String KEY = "key" ;
	public static final String EQ_SYMBOL = "=" ;
	
	public static String encrypt(String dataStr) {
		try {
			MessageDigest m = MessageDigest.getInstance("MD5");
			m.update(dataStr.getBytes("UTF8"));
			byte s[] = m.digest();
			String result = "";
			for (int i = 0; i < s.length; i++) {
				result += Integer.toHexString((0x000000FF & s[i]) | 0xFFFFFF00).substring(6);
			}
			return StringUtils.upperCase(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public static String encryptParam(Map<String, String> paramMap,String key){
		TreeMap<String, String> treeMap = new TreeMap<>();
		treeMap.putAll( paramMap );
		StringBuffer buffer = new StringBuffer();
		for(Map.Entry<String, String> entry : treeMap.entrySet()){
			buffer.append(entry.getKey()).append(EQ_SYMBOL).append(entry.getValue());
			buffer.append("&");
		}
		buffer.append(KEY).append(EQ_SYMBOL).append(key);
		return encrypt(buffer.toString()) ; 
	}
	
	
	public static String getUrl(Map<String, String> paramMap,String key){
		String sign = encryptParam(paramMap, key) ;
		StringBuffer buffer = new StringBuffer();
		for(Map.Entry<String, String> entry : paramMap.entrySet()){
			buffer.append(entry.getKey()).append(EQ_SYMBOL).append(entry.getValue());
			buffer.append("&");
		}
		buffer.append("sign").append(EQ_SYMBOL).append(sign);
		return buffer.toString();
	}
	
	public static String isVerification(Map<String, String> paramMap,String key){
		TreeMap<String, String> treeMap = new TreeMap<>();
		treeMap.putAll( paramMap );
		StringBuffer buffer = new StringBuffer();
		for(Map.Entry<String, String> entry : treeMap.entrySet()){
			buffer.append(entry.getKey()).append(EQ_SYMBOL).append(entry.getValue());
			buffer.append("&");
		}
		buffer.append(KEY).append(EQ_SYMBOL).append(key);
		
		return encrypt(buffer.toString()) ; 
	}
	
	
}