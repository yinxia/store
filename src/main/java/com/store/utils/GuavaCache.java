package com.store.utils;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public class GuavaCache {

	public static Cache<String, Object> COMM_CACHE = CacheBuilder.newBuilder().expireAfterAccess(30, TimeUnit.MINUTES)
			.maximumSize(1024).build();

	protected static Cache<String, Object> ONE_DAY_CACHE = CacheBuilder.newBuilder().expireAfterAccess(1, TimeUnit.HOURS)
			.maximumSize(1024).build();  

	public static Object loadCache(String cacheKey, Callable<Object> loader) {
		try {
			return COMM_CACHE.get(cacheKey , () -> loader.call());
		} catch (Exception e) {
		}
		return null;
	}

	public static void putCache(String key, Object value) {
		ONE_DAY_CACHE.put(key, value);
	}

	public static Object getCache(String key) {
		return ONE_DAY_CACHE.getIfPresent(key);
	}
	
}
