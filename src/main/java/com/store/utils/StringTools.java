package com.store.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.store.exceptions.ServiceRuntimeException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;

import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("unchecked")
public class StringTools {
	
	public static boolean isPhone(String phone) {
	    String regex = "^1\\d{10}";
	    if (phone.length() != 11) {
	        return false;
	    } else {
	        Pattern p = Pattern.compile(regex);
	        Matcher m = p.matcher(phone);
	        boolean isMatch = m.matches();
	        return isMatch;
	    }
	}
	
	public static String format(Object obj, int length) {
		String val = String.valueOf(null == obj ? 1 : obj);

		while (val.length() < length) {
			val = "0" + val;
		}
		return val;
	}

	public static String toString(Object value){
		if(null == value){
			return StringUtils.EMPTY ;
		}
		return value.toString() ;
	}

	static char[]chars = new char[26];
	static{
		for(int x=0;x<26;x++){
			chars[x]=(char)(65+x) ;
		}
	}

	public static String randomChar(int length) {
		String result = "" ;

		java.security.SecureRandom random = new java.security.SecureRandom();
		while(result.length() < length) {
			result += chars[random.nextInt(chars.length)];
		}
		return result ;
	}
	
	public static String randomInt(int length) {
		String result = "" ;

		java.security.SecureRandom random = new java.security.SecureRandom();
		while(result.length() < length) {
			result += random.nextInt(10);
		}
		return result ;
	}


	public static String toJSON(Object obj){
		if(ObjectUtils.isEmpty(obj)){
			return "{}";
		}

		if(obj instanceof Map<?,?>){
			Map<?,?> tempMap = (Map<?,?>) obj ;
			if(null != tempMap){
				Iterator<?> iterator = tempMap.entrySet().iterator() ;
				while(iterator.hasNext()){
					Map.Entry<? ,?> entry = (Map.Entry<? ,?>)iterator.next() ;
					if(entry.getValue() == null){
						iterator.remove();
					}
				}
			}
		}

		ObjectMapper map = new ObjectMapper();
		map.setSerializationInclusion(JsonInclude.Include.NON_NULL);

		try {
			return map.writeValueAsString(obj);
		}catch (Exception e){
			throw new ServiceRuntimeException(e.getMessage()) ;
		}
	}

	public static Map<String,Object> toMap(String json){
		ObjectMapper map = new ObjectMapper();
		try {
			return map.readValue( json , Map.class) ;
		}catch (Exception e){
			throw new ServiceRuntimeException(e.getMessage()) ;
		}
	}
	
	public static Long[]toLongs(String[]val){
		 Long[] arry = new  Long[] {};
		 for(String v:val) {
			 arry = 
					ArrayUtils.add(arry, ObjectTools.toLong(v)) ;
		 }
		 return arry;
	}

	public static String trimBearer(String token){
		return toString(token).replace("Bearer ", StringUtils.EMPTY).replace("Bearer", StringUtils.EMPTY);
	}
	
	public static String trim(String token){
		return toString(token).replace("\"","");
	} 
	
	public static int trimTime(String val){
		val = toString(val) ;
		if(val.indexOf("天") > -1) {
			return  ObjectTools.toInt(toString(val).replaceAll("天", "")) * 8 ;
		} 
		return ObjectTools.toInt(val.replaceAll("小时", "").replaceAll("时", "")) ; 
	}
	
	public static String getBetString(String ctype , String content , Long jobid) {
		return "< a ng-click=\"checkDetailFn(0,'','"+ctype+"','"+content+"','"+jobid+"')\">发起对赌</ a>" ;
	}
}