package com.store.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClazzUtils {
    /**
     * 获取某个类的所有属性,包括父类
     *
     * @param clazz
     * @return
     */
    public static List<Field> getFields(Class<?> clazz) {
        List<Field> fields = new ArrayList<>();
        while (null != clazz) {
            Field[] fies = clazz.getDeclaredFields();
            for(Field f : fies){
                if(Modifier.isStatic(f.getModifiers())){
                    continue;
                }
                fields.add( f );
            }
            clazz = clazz.getSuperclass();
            if (Object.class.equals(clazz) || null == clazz) {
                break;
            }
        }
        return fields;
    }
    /**
     * 获取某个类的所有方法,包括父类
     *
     * @param clazz
     * @return
     */
    public static List<Method> getMethods(Class<?> clazz) {
        List<Method> methods = new ArrayList<>();
        while (null != clazz) {
            methods.addAll(Arrays.asList(clazz.getDeclaredMethods()));
            clazz = clazz.getSuperclass();
            if (Object.class.equals(clazz) || null == clazz) {
                break;
            }
        }
        return methods;
    }
}