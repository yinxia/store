package com.store.utils;

import java.text.DecimalFormat;

public class DecimalFormatUtil {

	public static final String DOUBLE_FORMART_1 = "0.0";
	public static final String DOUBLE_FORMART_2 = "0.00";
	public static final String DOUBLE_FORMART_3 = "0.000";
	public static final String DOUBLE_FORMART_4 = "0.0000";
	public static final String DOUBLE_FORMART_5 = "0.00000";
	public static final String DOUBLE_FORMART_6 = "0.000000";

	public static String toFixed1(Double dou) {
		return toFixed(dou, DOUBLE_FORMART_1);
	}

	public static String toFixed2(Double dou) {
		return toFixed(dou, DOUBLE_FORMART_2);
	}

	public static String toFixed3(Double dou) {
		return toFixed(dou, DOUBLE_FORMART_3);
	}

	public static String toFixed4(Double dou) {
		return toFixed(dou, DOUBLE_FORMART_4);
	}

	public static String toFixed5(Double dou) {
		return toFixed(dou, DOUBLE_FORMART_5);
	}

	public static String toFixed(Double dou, String pattern) {
		double d = null == dou ? 0 : dou.doubleValue();
		DecimalFormat df = new DecimalFormat(pattern);
		return df.format(d);
	}
}