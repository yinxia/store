package com.store.utils;

import com.store.entity.StoreUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

//当前用户的上下文工具
public abstract class UserContext {

    //当前用户在session中的常量KEY
    public static final String USER_IN_SESSION = "USER_IN_SESSION";

    private static HttpSession getSession() {
        return ((ServletRequestAttributes) (RequestContextHolder.getRequestAttributes())).getRequest().getSession();
    }

    public static String getToken() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String token = request.getHeader(Constant.HEADER_AUTHORIZATION);
        if(StringUtils.isEmpty(token)){
            token = request.getParameter("access_token");

        }
        if(StringUtils.isEmpty(token)) {
            return null ;
        }
    	/*String[]array = token.split(" ");

    	return array.length > 1 ? array[1] : null;*/
        return  StringTools.trimBearer(token);
    }
    
    //设置当前登录的用户
    public static void setCurrentUser(StoreUser user) {
        getSession().setAttribute(USER_IN_SESSION,user);
    }

    //获取当前登录的用户
    public static StoreUser getCurrentUser() {
        return (StoreUser) getSession().getAttribute(USER_IN_SESSION);
    }

    /*public static Map<String, Object> getUser(){
        return JWT.parseToken(getToken());
    }*/
}
