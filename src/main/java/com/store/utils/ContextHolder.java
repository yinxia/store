package com.store.utils;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName: ContextHolder
 * @Author: 黄飞
 * @Date: 2018/6/26 16:48
 * @Desription: ${}
 * @Copy: ${com.bjike}
 **/
public class ContextHolder {

    public static String getRequestParam(String name){
        return getRequest().getParameter(  name ) ;
    }

    public static HttpServletRequest getRequest(){
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        return request ;
    }

}
