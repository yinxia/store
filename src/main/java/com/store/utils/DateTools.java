package com.store.utils;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Date;

public class DateTools {

	public static Date parseDate(final String str, final String... parsePatterns)  {
		try {
			return DateUtils.parseDate(str, parsePatterns);
		} catch (Exception e) {
		}
		return null;
	}
	
	public static String toDateString(final String str){
		try {
			Date parseDate = parseDate(str, "yyyyMMdd" , "yyyy/MM/dd" , "yyyy-MM-dd") ;
			
			return DateFormatUtils.format(parseDate, "yyyy-MM-dd") ;
		} catch (Exception e) {
		}
		return str ; 
	}
	
}
