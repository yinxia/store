package com.store.enums;


import com.store.base.BaseEnum;

import javax.persistence.AttributeConverter;

/**
 * @ClassName: JoType
 * @Author:
 * @Date: 18-8-23 上午9:12
 * @Desription: ${}
 * @Copy: ${com.bjike}
 **/
public enum JoType implements BaseEnum {

    
    JOURNALISM(0,"公司新闻"),

    INFORMATION(1,"行业资讯");

    private int code;

    private String message;

    JoType(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static class Convert implements AttributeConverter<JoType, Integer> {
        @Override
        public Integer convertToDatabaseColumn(JoType bettype) {
            return bettype == null ? null : bettype.getCode();
        }

        @Override
        public JoType convertToEntityAttribute(Integer dbData) {
            if(null == dbData){return null;}
            for (JoType type : JoType.values()) { //将数字转换为描述
                if (dbData.equals(type.getCode())) {
                    return type;
                }
            }
            return null;
        }
    }
}
