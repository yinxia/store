package com.store.enums;


import com.store.base.BaseEnum;

import javax.persistence.AttributeConverter;

public enum StaffSize implements BaseEnum {
    COMMUNICATION(0 , "通信") ,
    MARKETING(1,"营销") ,
    SOFTWARE(2 , "软件研发") ,
    INTERNET_OF_THINGS( 3 , "物联网") ;

    private int code;

    private String message;

    StaffSize(int code, String text) {
        this.code = code;
        this.message = text;
    }

    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public static class Convert implements AttributeConverter<StaffSize, Integer> {
        @Override
        public Integer convertToDatabaseColumn(StaffSize betsate) {
            return betsate == null ? null : betsate.getCode();
        }

        @Override
        public StaffSize convertToEntityAttribute(Integer dbData) {
            if (null == dbData) {
                return null;
            }
            for (StaffSize type : StaffSize.values()) { //将数字转换为描述
                if (dbData.equals(type.getCode())) {
                    return type;
                }
            }
            return null;
        }
    }
}
