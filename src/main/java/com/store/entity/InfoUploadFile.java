package com.store.entity;

import com.store.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "uploadfile")
@Data
@EqualsAndHashCode
public class InfoUploadFile extends BaseModel {


    @Column(name = "val", columnDefinition = "VARCHAR(255)   COMMENT '备注'")
    private String val;

    @Column(name = "path", columnDefinition = "VARCHAR(255)   COMMENT '存放路径'")
    private String path;

    @Column(name = "file_name", columnDefinition = "VARCHAR(255)   COMMENT '文件名'")
    private String name;

    @Column(name = "type", columnDefinition = "VARCHAR(255)   COMMENT '上传类型'")
    private String type;

    @Column(name = "size", columnDefinition = "decimal(10,2)   COMMENT '文件大小'")
    private double size;

    @Column(name = "info_id", columnDefinition = "bigint(20)   COMMENT '信息表id'")
    private Long infoId;


    @Transient
    private MultipartFile file;
}
