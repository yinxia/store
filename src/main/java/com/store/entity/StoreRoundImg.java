package com.store.entity;

import com.store.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @ClassName StoreRoundImg
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/5/27 0027 10:58
 * @Version 1.0
 */
@Entity
@Table(name = "store_round_img")
@Data
@EqualsAndHashCode(callSuper = false)
public class StoreRoundImg extends BaseModel {

    @Column(name = "img_id", columnDefinition = "bigint(20)   COMMENT '图片id'")
    private Long imgId;

    @Column(name = "img_link", columnDefinition = "VARCHAR(200) COMMENT '图片链接'")
    private String imgLink;

    @Column(name = "is_show",columnDefinition = "VARCHAR(200) COMMENT '是否显示'")
    private String isShow;


    @Transient
    private InfoUploadFile infoUploadFile;

}
