package com.store.entity;

import com.store.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @ClassName Menu
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/29 0029 14:47
 * @Version 1.0
 */
@Entity
@Table(name = "xy_menu")
@Data
@EqualsAndHashCode
public class Menu extends BaseModel {

    @Column(name = "superior_menu", columnDefinition = "bigint(20)   COMMENT '上级菜单'")
    private Long superiorMenu;

    @Column(name = "title", columnDefinition = "VARCHAR(255)   COMMENT '菜单名称'")
    private String title;

    @Column(name = "path", columnDefinition = "VARCHAR(255)   COMMENT '路径名称'")
    private String path;

    @Column(name = "name", columnDefinition = "VARCHAR(255)   COMMENT '组件名称'")
    private String name;

    @Column(name = "component", columnDefinition = "VARCHAR(255)   COMMENT '组件路径'")
    private String component;

    @Column(name = "keep_show", columnDefinition = "boolean   COMMENT '是否可见'")
    private Boolean keepShow;

    @Column(name = "level",columnDefinition = "bigint(20) COMMENT '菜单等级'")
    private Long level;

    @Column(name = "alltitle_num",  columnDefinition = "LONGTEXT  COMMENT '所有菜单层级'")
    private String allTitleNum;

    @Column(name = "icon", columnDefinition = "VARCHAR(255)   COMMENT '菜单图标'")
    private String icon;

    @Column(name = "redirect", columnDefinition = "VARCHAR(255)   COMMENT '指定页面'")
    private String redirect;

    @Column(name = "sorting", columnDefinition = "VARCHAR(50)   COMMENT '排序'")
    private String sorting;

}
