package com.store.entity;

import com.store.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @ClassName Address
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/5 0005 11:01
 * @Version 1.0
 */

@Entity
@Table(name = "address")
@Data
@EqualsAndHashCode
public class Address extends BaseModel {


    @Column(name = "consignee", columnDefinition = "VARCHAR(56)   COMMENT '收货人'")
    private String consignee;

    @Column(name = "phone", columnDefinition = "VARCHAR(56)   COMMENT '手机号码'")
    private String phone;

    @Column(name = "province", columnDefinition = "VARCHAR(50) COMMENT '省'")
    private String province;

    @Column(name = "city", columnDefinition = "VARCHAR(50) COMMENT '市'")
    private String city;

    @Column(name = "area", columnDefinition = "VARCHAR(50) COMMENT '区'")
    private String area;

    @Column(name = "detailed_address", columnDefinition = "VARCHAR(255) COMMENT '详细地址'")
    private String detailedAddress;


}
