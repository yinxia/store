package com.store.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.store.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

/**
 * @ClassName Order
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/5 0005 10:57
 * @Version 1.0
 */
@Entity
@Table(name = "xy_order")
@Data
@EqualsAndHashCode
public class Order extends BaseModel {

    @Column(name = "address_id", columnDefinition = "bigint(20)  COMMENT '收货地址id'")
    private Long addressId;

    @Column(name = "preferential_id", columnDefinition = "bigint(20)   COMMENT '优惠id'")
    private Long preferentialId;

    @Column(name = "full_reduction", columnDefinition = "VARCHAR(20)   COMMENT '支付状态 1.已支付，2.待支付，3.已完成'")
    private String fullReduction;

    @Column(name = "haveread", columnDefinition = "VARCHAR(20)   COMMENT '是否已读 1.已读，2.未读'")
    private String haveRead;

    @Column(name = "send_time", columnDefinition = "DATETIME COMMENT '寄送时间'")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date sendTime;

    @Column(name = "courier_number", columnDefinition = "VARCHAR(255)   COMMENT '快递单号'")
    private String courierNumber;

    @Transient
    private List<OrderWares> orderWaresLists;

    @Transient
    private String consignee;

    @Transient
    private String phone;

    @Transient
    private String detailedAddress;

    @Transient
    private String fullReductionName;


}
