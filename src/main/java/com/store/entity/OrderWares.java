package com.store.entity;

import com.store.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @ClassName OrderWares
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/6 0006 09:48
 * @Version 1.0
 */
@Entity
@Table(name = "order_wares")
@Data
@EqualsAndHashCode
public class OrderWares extends BaseModel {


    @Column(name = "order_id", columnDefinition = "bigint(20)   COMMENT '订单id'")
    private Long orderId;

    @Column(name = "wares_id", columnDefinition = "bigint(20)   COMMENT '商品'")
    private Long waresId;

    @Column(name = "num", columnDefinition = "bigint(20)   COMMENT '数量'")
    private int num;

    @Transient
    private String productName;

    @Transient
    private String taste;

}
