package com.store.entity;

import com.store.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @ClassName RoleOrMenu
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/29 0029 14:47
 * @Version 1.0
 */
@Entity
@Table(name = "xy_role_menu")
@Data
@EqualsAndHashCode
public class RoleOrMenu extends BaseModel {


    @Column(name = "role_id", columnDefinition = "bigint(20)   COMMENT '角色id'")
    private Long roleId;


    @Column(name = "menu_id", columnDefinition = "bigint(20)   COMMENT '菜单id'")
    private Long menuId;

    /**
     * 是否可新增
     */
    @Column(name = "addable", columnDefinition = "VARCHAR(20)   COMMENT '是否可新增'")
    private String addable;

    /**
     * 是否可编辑
     */
    @Column(name = "editable", columnDefinition = "VARCHAR(20)   COMMENT '是否可编辑'")
    private String editable;

    /**
     * 是否可删除
     */
    @Column(name = "deletable", columnDefinition = "VARCHAR(20)  COMMENT '是否可删除'")
    private String deletable;

    /**
     * 是否可查找
     */
    @Column(name = "selectable", columnDefinition = "VARCHAR(20)   COMMENT '是否可查找'")
    private String selectable;

    /**
     * 是否可导出
     */
    @Column(name = "exportable", columnDefinition = "VARCHAR(20)   COMMENT '是否可导出'")
    private String exportable;



}
