package com.store.entity;

import com.store.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @ClassName Role
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/16 0016 17:05
 * @Version 1.0
 */
@Entity
@Table(name="xy_role")
@Data
@EqualsAndHashCode
public class Role extends BaseModel {

    @Column( name = "role"  , columnDefinition = "VARCHAR(255)   COMMENT '用户角色'")
    private String role;

    @Column( name = "role_name"  , columnDefinition = "VARCHAR(255)   COMMENT '角色名称'")
    private String roleName;

    @Column( name = "user_id"  , columnDefinition = "bigint(20)   COMMENT '用户id'")
    private Long userId;

}
