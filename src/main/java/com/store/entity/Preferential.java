package com.store.entity;

import com.store.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @ClassName preferential
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/5 0005 10:33
 * @Version 1.0
 */

@Entity
@Table(name = "preferential")
@Data
@EqualsAndHashCode
public class Preferential extends BaseModel {


    @Column(name = "preferential_name", columnDefinition = "VARCHAR(255)   COMMENT '优惠名称'")
    private String preferentialName;

    @Column(name = "money", columnDefinition = "VARCHAR(56)   COMMENT '金额'")
    private String money;


}
