package com.store.entity;

import com.store.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @ClassName Permissions
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/16 0016 17:10
 * @Version 1.0
 */
@Entity
@Table(name="xy_permissions")
@Data
@EqualsAndHashCode
public class Permissions extends BaseModel {

    @Column( name = "permissions_name"  , columnDefinition = "VARCHAR(56)   COMMENT '用户权限'")
    private String permissionsName;

    @Column( name = "role_id"  , columnDefinition = "bigint(20)   COMMENT '角色id'")
    private Long roleId;

}
