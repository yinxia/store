package com.store.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.store.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

/**
 * @ClassName Wares
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/5/27 0027 09:56
 * @Version 1.0
 */
@Entity
@Table(name="xy_wares")
@Data
@EqualsAndHashCode
public class Wares extends BaseModel {


    @Column(name ="product_name",columnDefinition = "VARCHAR(200) COMMENT '产品名'")
    private String productName;

    @Column(name ="product_introduction",columnDefinition = "VARCHAR(200) COMMENT '商品简介'")
    private String productIntroduction;

    @Column(name ="cover",columnDefinition = "VARCHAR(200) COMMENT '商品封面'")
    private String cover;

    @Column(name ="classification",columnDefinition = "VARCHAR(56) COMMENT '商品分类'")
    private String classification;

    @Column(name ="price",columnDefinition = "VARCHAR(200) COMMENT '商品价格'")
    private String price;

    @Column(name ="taste",columnDefinition = "VARCHAR(56) COMMENT '口味'")
    private String taste;

    @Column(name ="specifica_tions",columnDefinition = "VARCHAR(56) COMMENT '规格'")
    private String specifications;

    @Column(name ="info_name",columnDefinition = "VARCHAR(200) COMMENT '购买信息名'")
    private String infoName;

    @Column(name ="product_introduction2",columnDefinition = "VARCHAR(200) COMMENT '产品介绍'")
    private String productIntroduction2;

    @Column(name = "pull_time", columnDefinition = "DATETIME COMMENT '上架时间'")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date pullTime;

    @Column(name = "putTime", columnDefinition = "DATETIME COMMENT '下架时间'")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date putTime;

    @Column(name = "detailsfigure", columnDefinition = "VARCHAR(255)  COMMENT '详情图'")
    private String detailsFigureId;

    @Column(name = "sorts", columnDefinition = "VARCHAR(56)  COMMENT '排序'")
    private String sorts;

    @Column(name = "put_pull_shelves", columnDefinition = "VARCHAR(56)  COMMENT '1.上架 2.下架'")
    private String putPullShelves;

    @Column(name = "preferential_id", columnDefinition = "VARCHAR(20)  COMMENT '优惠设置id'")
    private Long preferentialId;

    //详情图
    @Transient
    private List<InfoUploadFile> detailsFigureFiles;

    @Transient
    private InfoUploadFile covers;

}
