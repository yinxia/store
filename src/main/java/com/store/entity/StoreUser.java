package com.store.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.store.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

/**
 * @ClassName: user
 * @Author: pengyongliang
 * @Date: 19-6-25 下午3:42
 * @Desription: 用户
 **/
@Entity
@Table(name="xy_user")
@Data
@EqualsAndHashCode
public class StoreUser extends BaseModel {

    @Column(name = "phone", columnDefinition = "VARCHAR(255)   COMMENT '手机'")
    private String phone;

    @Column(name ="password" ,columnDefinition = "VARCHAR(200) COMMENT '密码'")
    private String password;

    @Column(name ="u_name",columnDefinition = "VARCHAR(200) COMMENT '用户名'")
    private String uName;

    @Column( name = "login_time", columnDefinition = "TIMESTAMP  DEFAULT CURRENT_TIMESTAMP")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date loginTime;

    //头像
    @Column(name = "avatar", columnDefinition = "varchar(200) comment '头像'")
    private String avatar;

    @Column(name = "user_type", columnDefinition = "varchar(200) comment '用户类型'")
    private  String userType;

    @Transient
    private String smsCode;

    @Transient
    private String passWord2;


}
