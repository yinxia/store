package com.store.controller;

import com.store.beans.ApiResult;
import com.store.entity.StoreRoundImg;
import com.store.pager.Pager;
import com.store.services.InfoUploadFileService;
import com.store.services.StoreRoundImgService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @ClassName StoreRoundImgController
 * @function  轮播图管理
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/5/27 0027 11:46
 * @Version 1.0
 */

@RestController
@RequestMapping("/api/storeRoundImg")
@Log4j
public class StoreRoundImgController {

    @Autowired
    private StoreRoundImgService service;

    @Autowired
    private InfoUploadFileService fileService;

    /**
     * 保存
     * @param file
     * @param storeRoundImg
     * @return
     */
    @PostMapping("/saveOrUpdate")
    public ApiResult<?> saveOrUpdate(MultipartFile file, StoreRoundImg storeRoundImg){
        try{
            return ApiResult.prepare(service.saveOrUpdate(file,storeRoundImg));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }


    /**
     * 列表
     * @param pager
     * @return
     */
    @GetMapping("/getPagerAllList")
    public ApiResult<?> getPagerAllList(Pager<StoreRoundImg> pager){
        try{
            return ApiResult.prepare(service.findPage(pager));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    /**
     * 详情
     * @param id
     * @return
     */
    @GetMapping("/details")
    public ApiResult<?> details(Long id){
        try{
            return ApiResult.prepare(service.details(id));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }


    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping("/del")
    public ApiResult<?> del(Long id){
        try{
            StoreRoundImg storeRoundImg = service.findOne(id);
            if(null != storeRoundImg){
                Long [] longs = {storeRoundImg.getImgId()};
                fileService.deletes(longs);
                service.delete(id);
            }
            return ApiResult.prepare("删除成功。");
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }


    @PostMapping("/changeState")
    public ApiResult<?> changeState(Long id){
        StoreRoundImg roundImg=service.findOne(id);
        roundImg.setIsShow(roundImg.getIsShow().equals("1") ? "2" : "1" );
        return ApiResult.prepare(service.update(roundImg));
    }



}
