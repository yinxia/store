package com.store.controller;

import com.store.beans.ApiResult;
import com.store.entity.Permissions;
import com.store.services.PermissionsService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName PermissionsController
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/16 0016 17:42
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/permissions")
@Log4j
public class PermissionsController {

    @Autowired
    private PermissionsService permissionsService;

    @PostMapping("/save")
    public ApiResult<?> savePermissions(Permissions permissions){
       return ApiResult.prepare(permissionsService.saveOrUpdate(permissions));
    }
}
