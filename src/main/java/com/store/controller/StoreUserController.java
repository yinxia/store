package com.store.controller;

import com.store.beans.ApiResult;
import com.store.entity.StoreUser;
import com.store.services.StoreUserService;
import com.store.utils.*;
import lombok.extern.log4j.Log4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName StoreUserController
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/5/26 0026 11:33
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/user")
@Log4j
public class StoreUserController {


    @Autowired
    protected SmsService smsService;

    @Autowired
    private StoreUserService service;



    /**
     * 注册
     * @param storeUser
     * @return
     */
    @PostMapping("/pub/register")
    public ApiResult<?> register(StoreUser storeUser){

        try {
            /*
            if (StringUtils.isEmpty(storeUser.getSmsCode())) {
                return ApiResult.prepareError("验证码不能为空");
            }

            if (!StringUtils.equalsAny(storeUser.getSmsCode(),
                    ObjectTools.toString(redisService.get(storeUser.getPhone())))) {
                return ApiResult.prepareError("验证码输入错误");
            }*/
            return ApiResult.prepare(service.register(storeUser));

        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    /**
     * 登录
     * @return
     */
    /*@PostMapping("/login")
    public ApiResult login(String phone, String password) {

        Map<String, Object> map = new HashMap<>();
        StoreUser user = service.findByphone(phone);
        if(null == user){
            *//* 账号不存在 *//*
            map.put("code", 3);
            map.put("error", "invalid_grant");
            map.put("error_description", "Bad credentials");
            map.put("msg", "账号不存在");
            return ApiResult.prepareError("账号不存在");
        }
        String encryptPwd = MD5.md5(password);
        if (!StringUtils.equalsIgnoreCase(encryptPwd, user.getPassword())) {
            map.put("code", 3);
            map.put("msg", "账号或者密码错误");
            map.put("error", "invalid_grant");
            map.put("error_description", "Bad credentials");
            return ApiResult.prepareError("账号或者密码错误");
        }
        map.put("uName", user.getUName());
        map.put("phone", user.getPhone());
        map.put("token_type", "bearer");
        String accessToken = JWT.createJWT(user.getId(),phone, password);
        map.put("code", 0);
        map.put("access_token", accessToken);
        map.put("refresh_token", accessToken);
        map.put("expires_in", JWT.TTL_MILLIS);

        service.updateLoginTime(new Date(),phone);

        return ApiResult.prepare(map);
    }*/

    @RequestMapping("/login")
    public ApiResult<?> login(StoreUser storeUser){
        Subject subject = SecurityUtils.getSubject();
        try {
            if (!subject.isAuthenticated()) {
                System.out.println(MD5.md5(storeUser.getPassword()));
                UsernamePasswordToken token = new UsernamePasswordToken(storeUser.getPhone(), storeUser.getPassword());
                //token.setRememberMe(user.isRememberMe());
                SecurityUtils.getSubject().login(token);
                Session session = subject.getSession();
                session.setAttribute("subject",storeUser);
                session.setTimeout(-10001);
            }
        } catch (Exception e) {
            return ApiResult.prepareError(e.getMessage());
        }

        // 编码方式判断是否具有管理员身份
        if (subject.hasRole("admin")) {
            return ApiResult.prepare("有admin权限");
        }
        return ApiResult.prepare("无admin权限");
    }





    @GetMapping("/index")
    @RequiresPermissions("user:add")
    public Map<String,Object> getAdminInfo(){
        Map<String,Object> map = new HashMap<>();
        map.put("code",200);
        map.put("msg","这里是只有user:add");
        return map;
    }


    @RequestMapping("/login2")
    public Map<String,Object> login2(StoreUser storeUser){
        Map<String,Object> map = new HashMap<>();
        //进行身份验证
        try{
            //验证身份和登陆
            Subject subject = SecurityUtils.getSubject();
            UsernamePasswordToken token = new UsernamePasswordToken(storeUser.getPhone(), storeUser.getPassword());
            //进行登录操作
            subject.login(token);
        }catch (IncorrectCredentialsException e) {
            map.put("code",500);
            map.put("msg","用户不存在或者密码错误");
            return map;
        } catch (AuthenticationException e) {
            map.put("code",500);
            map.put("msg","该用户不存在");
            return map;
        } catch (Exception e) {
            map.put("code",500);
            map.put("msg","未知异常");
            return map;
        }
        map.put("code",0);
        map.put("msg","登录成功");
        map.put("token",ShiroUtils.getSession().getId().toString());
        return map;
    }



    @GetMapping("/403")
    public ApiResult err(){
        return ApiResult.prepareError("403");
    }
}
