package com.store.controller;

import com.store.beans.ApiResult;
import com.store.entity.Address;
import com.store.pager.Pager;
import com.store.services.AddressService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName AddressController
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/6 0006 09:42
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/address")
@Log4j
public class AddressController {

    @Autowired
    private AddressService service;

    @PostMapping("/saveOrUpdate")
    public ApiResult<?> saveOrUpdate(Address address) {
        try {
            return ApiResult.prepare(service.saveOrUpdate(address));
        } catch (Exception e) {
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @GetMapping("/details")
    public ApiResult<?> details(Long id) {
        try {
            return ApiResult.prepare(service.findOne(id));
        } catch (Exception e) {
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @DeleteMapping("/del")
    public ApiResult<?> del(Long id) {
        try {
            service.delete(id);
            return ApiResult.prepare("删除成功。");
        } catch (Exception e) {
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @GetMapping("/getPagerAllList")
    public ApiResult<?> getPagerAllList(Pager<Address> pager) {
        try {
            return ApiResult.prepare(service.findPager(pager));
        } catch (Exception e) {
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }
}
