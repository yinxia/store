package com.store.controller;

import com.store.beans.ApiResult;
import com.store.entity.Wares;
import com.store.pager.Pager;
import com.store.services.WaresService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName WaresController
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/4 0004 18:13
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/wares")
@Log4j
public class WaresController {

    @Autowired
    private WaresService service;


    @PostMapping("/saveOrUpdate")
    public ApiResult<?> saveOrUpdate(Wares wares){
        try{
            return ApiResult.prepare(service.saveOrUpdate(wares));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }


    @GetMapping("/details")
    public ApiResult<?> details(Long id){
        try{
            return ApiResult.prepare(service.details(id));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @DeleteMapping("/del")
    public ApiResult<?> del(Long id){
        try {
            Wares wares = service.findOne(id);
            if(null != wares){
                //照片id
                if(null != wares.getDetailsFigureId()) {
                    service.deleteFile(wares.getDetailsFigureId());
                }

                //删除封面id
                if(null != wares.getCover()) {
                    service.deleteFile(wares.getCover());
                }
            }
            service.delete(id);
            return ApiResult.prepare("删除成功。");
        }catch (Exception e){
            log.error("错误日志: " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }


    @GetMapping("/getPagerAllList")
    public ApiResult<?> getPagerAllList(Pager<Wares> pager, Wares wares){
        try{
            return ApiResult.prepare(service.getPagerAllList(pager,wares));
        }catch (Exception e){
            log.error("错误日志: " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }




}
