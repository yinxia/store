package com.store.controller;

import com.store.beans.ApiResult;
import com.store.entity.Order;
import com.store.pager.Pager;
import com.store.services.OrderService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @ClassName OrderService
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/6 0006 10:37
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/order")
@Log4j
public class OrderController {

    @Autowired
    private OrderService orderService;


    @PostMapping("/saveOrUpdate")
    public ApiResult<?> saveOrUpdate(String [] strs, Order order){
        try{
            return ApiResult.prepare(orderService.saveOrUpdate(strs,order));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @GetMapping("/details")
    public ApiResult<?> details(Long id) {
        try {
            return ApiResult.prepare(orderService.details(id));
        } catch (Exception e) {
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @DeleteMapping("/del")
    public ApiResult<?> del(Long id) {
        try {
            orderService.del(id);
            return ApiResult.prepare("删除成功。");
        } catch (Exception e) {
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @GetMapping("/getPagerAllList")
    public ApiResult<?> getPagerAllList(Pager<Order> pager, Order order, Date startTime, Date endTime,Date sendStartTime,Date sendEndTime) {
        try {
            return ApiResult.prepare(orderService.getPagerAllList(pager,order,startTime,endTime,sendStartTime,sendEndTime));
        } catch (Exception e) {
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

}
