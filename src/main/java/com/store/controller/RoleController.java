package com.store.controller;

import com.store.beans.ApiResult;
import com.store.entity.Role;
import com.store.services.RoleService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName RoleController
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/16 0016 17:31
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/role")
@Log4j
public class RoleController {

    @Autowired
    private RoleService roleService;


    @PostMapping("/save")
    public ApiResult<?> saveRole(){
        Role role = new Role();
        role.setRoleName("管理员");
        role.setRole("admin");
        roleService.save(role);
        return ApiResult.prepare(role);
    }

    @PostMapping("/saveOrUpdate")
    public ApiResult<?> saveOrUpdate(String json){
        try {
            return ApiResult.prepare(roleService.saveOrUpdate(json));
        }catch (Exception e){
            log.error("错误日志：" + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @GetMapping("/findRoles")
    public ApiResult<?> findRoles(){
        return ApiResult.prepare(roleService.findRoles());
    }
}
