package com.store.controller;

import com.store.beans.ApiResult;
import com.store.entity.InfoUploadFile;
import com.store.services.InfoUploadFileService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * @ClassName InfoUploadFileController
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/5/27 0027 14:18
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/infouploadFile")
@Log4j
public class InfoUploadFileController {

    @Autowired
    private InfoUploadFileService fileService;


    @Value("${updateDir:}")
    private String updateDir;

    /**
     * 文件上传
     * @param file
     * @param val
     * @param path
     * @param mainId
     * @return
     */
    @PostMapping("/uploadFile")
    public ApiResult<?> uploadFile(MultipartFile file,String val,String path,Long mainId){
        try{
            InfoUploadFile fileVo = fileService.upload2(file, val,path, mainId);
            return ApiResult.prepare(fileVo);
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    /**
     * 文件删除
     * @param ids
     * @return
     */
    @DeleteMapping("/del")
    public ApiResult<?> del(Long[] ids){
        try{
            fileService.deletes(ids);
            return ApiResult.prepare("删除成功。");
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }



    @GetMapping("/pub/img")
    public void show(Long id, HttpServletResponse response) throws Exception {
        InfoUploadFile attachment = fileService.findOne(id);
        if (null != attachment) {
            String filePath = attachment.getPath();
            File file = new File(filePath);
            log.error("file : " + file);
            if (!file.exists()) {
                log.error("找不到路径。");
                response.sendError(404);
                return;
            }
            FileInputStream fileInputStream = new FileInputStream(file);//与根据File类对象的所代表的实际文件建立链接创建fileInputStream对象
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Content-Length", String.valueOf(file.length()));
            response.setHeader("Content-Type", "image/png");
            org.apache.commons.io.IOUtils.copy(fileInputStream, response.getOutputStream());



        }
    }
}
