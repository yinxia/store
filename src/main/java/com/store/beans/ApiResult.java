package com.store.beans;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude( value = JsonInclude.Include.NON_NULL)
public class ApiResult<T> implements Serializable {
	
	private static final long serialVersionUID = 6714463778191735515L;

	private int code;
	
	private String msg ; 
	
	private T data ; 

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	} 

	public void set(int code , String msg){
		setCode(code);
		setMsg(msg) ;
	}

	public static <T> ApiResult<T> prepare(T data){
		ApiResult<T> result = new ApiResult<>();
		result.set(0 , "成功");
		result.setData(data);
		return result;
	}

	public static <T> ApiResult<T> prepare(){
		ApiResult<T> result = new ApiResult<>();
		return result;
	}
	public static ApiResult<String> prepareString(String data){
		ApiResult<String> result = new ApiResult<>();
		result.set(0 , data);
		return result;
	}

	public static <T> ApiResult<T> prepareError(T data){
		ApiResult<T> result = new ApiResult<>();
		result.set(1 , "失败");
		result.setData(data);
		return result;
	}
	public static <T> ApiResult<T> prepareError(String msg){
		ApiResult<T> result = new ApiResult<>();
		result.set(1 , msg);
		return result;
	}


	@Override
	public String toString() {
		return "ApiResult{" +
				"code=" + code +
				", msg='" + msg + '\'' +
				", data=" + data +
				'}';
	}
	
}
