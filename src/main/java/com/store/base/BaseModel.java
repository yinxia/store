package com.store.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.store.id.LongJsonDeserializer;
import com.store.id.LongJsonSerializer;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@JsonInclude( content = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties("handler")
public class BaseModel implements Serializable ,Cloneable {
	
	private static final long serialVersionUID = 4575448585184522706L;

	@JsonSerialize(using = LongJsonSerializer.class)
	@JsonDeserialize(using = LongJsonDeserializer.class)
	@Id
	@GeneratedValue(generator = "snowFlakeId")
	@GenericGenerator(name = "snowFlakeId", strategy = "com.store.id.SnowflakeId")
	@Column(name = "id" , columnDefinition = "bigint comment 'id主键'" , updatable = false)
	protected Long id ;
	 
	@Column( name = "create_date" ,  columnDefinition = "TIMESTAMP  DEFAULT CURRENT_TIMESTAMP")
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	protected Date createDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column( name = "create_by",columnDefinition = "bigint  DEFAULT 0 ")
	protected Long createBy;

	@Column( name = "update_date"  , columnDefinition = "TIMESTAMP  DEFAULT CURRENT_TIMESTAMP")
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	protected Date updateDate;

	@Column( name = "update_by" ,  columnDefinition = "bigint  DEFAULT 0 ")
	protected Long updateBy;

	@Column( name = "status"  , columnDefinition = "bigint  DEFAULT 0 ")
	protected int status;

	public Long getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Long createBy) {
		this.createBy = createBy;
	}

	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Long updateBy) {
		this.updateBy = updateBy;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column( name = "note" ,  columnDefinition = "varchar(255)  COMMENT '备注'")
	protected String note;

	public String getNote() {
		return note;
	}

	public void setNote(String renotemarks) {
		this.note = note;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
