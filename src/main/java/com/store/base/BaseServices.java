package com.store.base;

import com.store.entity.StoreUser;
import com.store.pager.Condition;
import com.store.pager.Pager;
import com.store.pager.RestrictionType;
import com.store.utils.BeanTransform;
import com.store.utils.ObjectTools;
import com.store.utils.UserContext;
import com.store.exceptions.NoIdRuntimeException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ObjectUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class BaseServices<T extends BaseModel, ID extends Serializable> {

	private BaseRepository<T, ID> repository;

	@Autowired
	protected EntityManager entityManager;

	/**
	 * 实体类类型
	 */
	private Class<T> entityClass;
	/**
	 * ID类类型
	 */
	protected Class<T> idClass;

	public BaseServices(BaseRepository<T, ID> repository) {
		this.repository = repository;

		entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		idClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
	}

	public T save(T entity) {
		entity.setCreateDate(new Date());
		entity.setUpdateDate(new Date());
		//Map<String, Object> getUserInfo = UserContext.getUser();
		//entity.setCreateBy(ObjectTools.toLong(getUserInfo.get("id")));
		//entity.setUpdateBy(ObjectTools.toLong(getUserInfo.get("id")));
		//entity.setCreateBy(123L);
		//entity.setUpdateBy(123L);
		entity.setCreateDate(new Date());
		entity.setUpdateDate(new Date());
		return repository.save(entity);
	}

	public T update(T entity) {
		if (null == entity.getId()) {
			throw new NoIdRuntimeException("没有设置ID");
		}
		if (null == entity.getId()) {
			throw new NoIdRuntimeException("没有设置ID");
		}
		//entity.setUpdateBy(UserContext.getCurrentUser().getId());
		/*WebsiteUser user = UserContext.getCurrentUser();
		if(user == null){
			user = new WebsiteUser();
			user.setId(123l);
		}
		entity.setCreateDate(new Date());
		entity.setUpdateDate(new Date());
		entity.setCreateBy(user.getId());
		entity.setUpdateBy(user.getId());
		entity.setStatus(0);*/
		/*Map<String, Object> getUserInfo = UserContext.getUser();
		entity.setCreateBy(ObjectTools.toLong(getUserInfo.get("id")));
		entity.setUpdateBy(ObjectTools.toLong(getUserInfo.get("id")));*/
		entity.setCreateDate(new Date());
		entity.setUpdateDate(new Date());
		return repository.save(entity);
	}

	public T saveOrUpdate(T entity) {
		/*WebsiteUser user = UserContext.getCurrentUser();
		if(user == null){
			user = new WebsiteUser();
			user.setId(123l);
			user.setLoginName("admin");
			entity.setCreateDate(new Date());
		}
		entity.setCreateDate(new Date());
		entity.setUpdateDate(new Date());
		entity.setCreateBy(user.getId());
		entity.setUpdateBy(user.getId());
		entity.setStatus(0);*/
		//Map<String, Object> getUserInfo = UserContext.getUser();
	/*	if (null == entity.getId() || entity.getId() == 0 ) {
			entity.setCreateDate(new Date());
			entity.setUpdateDate(new Date());
			entity.setCreateBy(ObjectTools.toLong(getUserInfo.get("id")));
			entity.setUpdateBy(ObjectTools.toLong(getUserInfo.get("id")));
		}*/
		entity.setCreateDate(new Date());
		entity.setUpdateDate(new Date());
		//entity.setCreateBy(ObjectTools.toLong(getUserInfo.get("id")));
		//entity.setUpdateBy(ObjectTools.toLong(getUserInfo.get("id")));
		return repository.saveAndFlush(entity);
	}


	public T findOne(ID id) {
		return repository.findOne(id);
	}

	public boolean exists(ID id) {
		return repository.exists(id);
	}

	public List<T> findAll() {
		return repository.findAll();
	}

	public List<T> findAll(ID[] ids) {
		return this.findAll(Arrays.asList(ids));
	}

	public List<T> findAll(List<ID> ids) {
		return repository.findAll(ids);
	}

	public long count() {
		return repository.count();
	}

	public void delete(ID id) {
		repository.delete(id);
	}

	public void delete(T entity) {
		repository.delete(entity);
	}


	public Page<T> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public List<T> findAll(Example<T> example, Sort sort) {
		return repository.findAll(example, sort);
	}

	public int updateHql(String hql, Object... params) {
		Query query = entityManager.createQuery(hql);
		if (!ArrayUtils.isEmpty(params)) {
			for (int x = 0; x < params.length; x++) {
				query.setParameter(x + 1, params[x]);
			}
		}
		return query.executeUpdate();
	}

	public int updateSql(String sql, Object... params) {
		Query query = entityManager.createNativeQuery(sql);
		if (!ArrayUtils.isEmpty(params)) {
			for (int x = 0; x < params.length; x++) {
				query.setParameter(x + 1, params[x]);
			}
		}
		return query.executeUpdate();
	}

	public List<Map<String, Object>> findListForSqlList(String sql, Object... params) {
		Query nativeQuery = entityManager.createNativeQuery(sql);
		nativeQuery.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		if (!ObjectUtils.isEmpty(params)){
			int index = 1;
			for (Object param : params) {
				nativeQuery.setParameter(index++, param);
			}
		}
		return nativeQuery.getResultList();
	}

	public List<T> findListForHql(String hql, int maxResult, Object... params) {
		Query query = entityManager.createQuery(hql, entityClass);
		query.setFirstResult(0);
		if (-1 != maxResult) {
			query.setMaxResults(maxResult);
		}
		if (!ObjectUtils.isEmpty(params)) {
			int index = 1;
			for (Object param : params) {
				query.setParameter(index++, param);
			}
		}
		return query.getResultList();
	}

	/**
	 * 自动拼接where查询子句
	 */
	protected void appendHsql(final Pager<T> pager, StringBuffer hql, StringBuffer countHql, boolean need) {
		int index = 0;
		for (Condition condition : pager.getConditions()) {

			if (hql.toString().toLowerCase().indexOf("where") == -1) {
				hql.append(" where 1=1 ");
				countHql.append(" where 1=1 ");
			}
			hql.append(" and ");
			countHql.append(" and ");

			if (condition.getRestrict() == RestrictionType.IN) {

				hql.append(condition.getField() + " " + condition.getRestrict().getSymbol() + "(");
				countHql.append(condition.getField() + " " + condition.getRestrict().getSymbol() + "(");
				int length = ObjectTools.getLength(condition.getValue());
				for (int i = 0; i < length; i++) {
					if (i > 0) {
						hql.append(",");
						countHql.append(",");
					}
					hql.append("?");
					countHql.append("?");
					if (need) {
						hql.append(String.valueOf((++index)));
						countHql.append(String.valueOf((index)));
					}
				}
				hql.append(")");
				countHql.append(")");
			} else {
				hql.append(condition.getField() + "  " + condition.getRestrict().getSymbol() + " ?");
				countHql.append(condition.getField() + "  " + condition.getRestrict().getSymbol() + "?");
				if (need) {
					hql.append(String.valueOf((++index)));
					countHql.append(String.valueOf((index)));
				}
			}
		}

		if (!ObjectUtils.isEmpty(pager.getOrders())) {
			hql.append(" order by ");
			for (Pager.Order order : pager.getOrders()) {
				hql.append(order.getName() + order.getOrderType().getSql());
			}
		}
	}

	public List<Map<String, Object>> findListSql(Pager<T> pager, String sql, String countSql, Object... params) {
		Session session = entityManager.unwrap(Session.class);
		StringBuffer hqlBuffer = new StringBuffer(sql);
		StringBuffer countBuffer = new StringBuffer(countSql);
		appendHsql(pager, hqlBuffer, countBuffer, true);

		SQLQuery query = session.createSQLQuery(hqlBuffer.toString());
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

		return query.list();
	}

	public Pager<T> findPagerByHql(final Pager<T> pager, Class<?> clazz, String hql, String countHql) {
		StringBuffer hqlBuffer = new StringBuffer(hql);
		StringBuffer countHqlBuffer = new StringBuffer(countHql);
		appendHsql(pager, hqlBuffer, countHqlBuffer, true);

		hql = hqlBuffer.toString();
		countHql = countHqlBuffer.toString();
		
		Query query = null;
		if (null != clazz) {
			query = entityManager.createQuery(hql, clazz);
		} else {
			query = entityManager.createQuery(hql);
		}
		query.setFirstResult((pager.getPage() - 1) * pager.getPageSize());
		query.setMaxResults(pager.getPageSize());
		setParamter(pager, query);

		List<T> data = query.getResultList();
		pager.setContent(data);

		Query countQuery = entityManager.createQuery(countHql);
		setParamter(pager, countQuery);
		long total = NumberUtils.toLong(String.valueOf(countQuery.getSingleResult()));
		pager.setTotalElements(total);
		pager.setSize(pager.getPageSize());
		pager.setTotalPages((pager.getTotalElements() - 1) / pager.getPageSize() + 1);
		pager.setFirst(pager.getPage() == 1);
		pager.setLast(pager.getPage() == pager.getTotalPages());
		return pager;
	}

	public Pager<Map<String, Object>> findPagerBySqlToMap(final Pager<T> pager, String sql, String countSql, boolean... isFlag) {
		StringBuffer hqlBuffer = new StringBuffer(sql);
		
		StringBuffer countHqlBuffer = new StringBuffer(countSql);
		if (isFlag.length == 0) {
			appendHsql(pager, hqlBuffer, countHqlBuffer, false);
		}
		sql = hqlBuffer.toString();
		countSql = countHqlBuffer.toString();

		Query query = entityManager.createNativeQuery(sql);
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		query.setFirstResult((pager.getPage() - 1) * pager.getPageSize());
		query.setMaxResults(pager.getPageSize());
		setParamter(pager, query);

		List<Map<String, Object>> data = query.getResultList();
		 
		Query countQuery = entityManager.createNativeQuery(countSql);
		setParamter(pager, countQuery);
		long total = NumberUtils.toLong(String.valueOf(countQuery.getSingleResult()));
		pager.setTotalElements(total);
		pager.setSize(pager.getPageSize());
		pager.setTotalPages((pager.getTotalElements() - 1) / pager.getPageSize() + 1);
		pager.setFirst(pager.getPage() == 1);
		pager.setLast(pager.getPage() == pager.getTotalPages());
		
		Pager<Map<String, Object>> result = pager.copy() ;
		result.setContent( data );
		
		return result;
	}
	
	public Pager<T> findPagerBySql(final Pager<T> pager, String sql, String countSql, boolean... isFlag) {
		StringBuffer hqlBuffer = new StringBuffer(sql);
		StringBuffer countHqlBuffer = new StringBuffer(countSql);
		if (isFlag.length == 0) {
			appendHsql(pager, hqlBuffer, countHqlBuffer, false);
		}
		sql = hqlBuffer.toString();
		countSql = countHqlBuffer.toString();

		Query query = entityManager.createNativeQuery(sql, entityClass);
		query.setFirstResult((pager.getPage() - 1) * pager.getPageSize());
		query.setMaxResults(pager.getPageSize());
		setParamter(pager, query);

		List<T> data = query.getResultList();
		pager.setContent(data);

		Query countQuery = entityManager.createNativeQuery(countSql);
		setParamter(pager, countQuery);
		long total = NumberUtils.toLong(String.valueOf(countQuery.getSingleResult()));
		pager.setTotalElements(total);
		pager.setSize(pager.getPageSize());
		pager.setTotalPages((pager.getTotalElements() - 1) / pager.getPageSize() + 1);
		pager.setFirst(pager.getPage() == 1);
		pager.setLast(pager.getPage() == pager.getTotalPages());
		return pager;
	}

	/**
	 * 设置参数
	 */
	protected void setParamter0(Pager<T> pager, Query query) {
		int forIndex = 0;
		for (int x = 0; x < pager.getConditions().size(); x++) {
			Condition condition = pager.getConditions().get(x);
			if (condition.getRestrict() == RestrictionType.IN) {
				int length = ObjectTools.getLength(condition.getValue());
				for (int i = 0; i < length; i++) {
					query.setParameter(forIndex++, ObjectTools.getObj(condition.getValue(), i));
				}
			} else if (RestrictionType.LIKE == condition.getRestrict()) {
				query.setParameter(forIndex++, "%" + condition.getValue() + "%");
			} else {
				query.setParameter(forIndex++, condition.getValue());
			}
		}
	}

	/**
	 * 设置参数
	 */
	protected void setParamter(Pager<T> pager, Query query) {
		int forIndex = 0;
		for (int x = 0; x < pager.getConditions().size(); x++) {
			Condition condition = pager.getConditions().get(x);
			if (condition.getRestrict() == RestrictionType.IN) {
				int length = ObjectTools.getLength(condition.getValue());
				for (int i = 0; i < length; i++) {
					query.setParameter(++forIndex, ObjectTools.getObj(condition.getValue(), i));
				}
			} else if (RestrictionType.LIKE == condition.getRestrict()) {
				query.setParameter(++forIndex, "%" + condition.getValue() + "%");
			} else {
				query.setParameter(++forIndex, condition.getValue());
			}
		}
	}

	public Pager<T> findPager(final Pager<T> pager) {
		Page<T> page = page(pager);
		pager.setTotalElements(page.getTotalElements());
		pager.setSize(page.getSize());
		pager.setFirst(page.isFirst());
		pager.setLast(page.isLast());
		pager.setTotalPages(page.getTotalPages());
		pager.setContent(page.getContent());
		return pager;
	}

	public List<T> findForCondition(List<Condition> list) {
		return findForCondition(list, new Pager.Order[] {});
	}

	public List<T> findForCondition(List<Condition> list, Pager.Order[] orders) {
		Condition[] conditions = new Condition[list.size()];
		list.toArray(conditions);
		return findForCondition(conditions, orders);
	}

	public T findForConditionOne(List<Condition> list) {
		Condition[] conditions = new Condition[list.size()];
		list.toArray(conditions);
		List<T> cons = findForCondition(conditions, new Pager.Order[] {});
		return ObjectUtils.isEmpty(cons) ? null : cons.get(0);
	}

	/**
	 * 根据查询条件拼接sql语句
	 */
	public List<T> findForCondition(Condition[] conditions, Pager.Order[] orders) {
		String hql = "from " + entityClass.getName();
		List<Object> params = new Vector<>();
		int index = 0;
		for (Condition condition : conditions) {
			if (hql.toLowerCase().indexOf("where") == -1) {
				hql += " where ";
			} else {
				hql += " and ";
			}
			hql += condition.getField() + " " + condition.getRestrict().getSymbol();
			if (condition.getRestrict() == RestrictionType.IN) {
				hql += " (";
				int len = ObjectTools.getLength(condition.getValue());
				for (int x = 0; x < len; x++) {
					if (x > 0) {
						hql += ",";
					}
					hql += "?" + ++index;
					params.add(ObjectTools.getObj(condition.getValue(), x));
				}
				hql += ")";
			} else if (condition.getRestrict() == RestrictionType.LIKE) {
				hql += "?" + ++index;
				params.add("%" + condition.getValue() + "%");
			} else {
				hql += "?" + ++index;
				params.add(condition.getValue());
			}
		}
		if (!ObjectUtils.isEmpty(orders)) {
			hql += " order by ";
			for (Pager.Order order : orders) {
				hql += order.getName() + " " + order.getOrderType();
			}
		}

		Query query = entityManager.createQuery(hql);
		index = 0;
		for (Object obj : params) {
			query.setParameter(index + 1, obj);
			index++;
		}

		return query.getResultList();
	}

	protected Page<T> page(final Pager<T> pager) {
		pager.addCondition(new Condition("status",0, RestrictionType.EQ));
		Pageable pageable = new PageRequest(pager.getPage() - 1, pager.getPageSize());
		return repository.findAll(new Specification<T>() {
			@Override
			public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate[] pre = new Predicate[pager.getConditions().size()];
				for (int x = 0; x < pager.getConditions().size(); x++) {
					Condition condition = pager.getConditions().get(x);
					switch (condition.getRestrict()) {
					case EQ:
						pre[x] = cb.equal(root.get(condition.getField()), condition.getValue());
						break;
					case LIKE:
						pre[x] = cb.like(root.get(condition.getField()), "%" + condition.getValue() + "%");
						break;
					case IN:
						// pre[x] =
						// cb.in(root.get(condition.getField())).value(condition.getValue())
						// ;
						In<Object> in = cb.in(root.get(condition.getField()));
						int length = ObjectTools.getLength(condition.getValue());
						if (length > 0) {
							for (int y = 0; y < length; y++) {
								in.value(ObjectTools.getObj(condition.getValue(), y));
							}
						}
						pre[x] = in;
						break;
					case GT:
						pre[x] = cb.greaterThan(root.get(condition.getField()), (Comparable) condition.getValue());
						break;
					case GTEQ:
						pre[x] = cb.greaterThanOrEqualTo(root.get(condition.getField()),
								(Comparable) condition.getValue());
						break;
					case LT:
						pre[x] = cb.lessThan(root.get(condition.getField()), (Comparable) condition.getValue());
						break;

					case LTEQ:
						pre[x] = cb.lessThanOrEqualTo(root.get(condition.getField()),
								(Comparable) condition.getValue());
						break;
					default:
						break;
					}
				}
				for (Pager.Order order : pager.getOrders()) {
					if (order.getOrderType() == Pager.OrderType.ASC) {
						query.orderBy(cb.asc(root.get(order.getName())));
					} else {
						query.orderBy(cb.desc(root.get(order.getName())));
					}
				}
				return query.where(pre).getRestriction();
			}
		}, pageable);
	}

	public void detach(T t) {
		entityManager.detach(t);
	}



}
