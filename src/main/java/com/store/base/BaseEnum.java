package com.store.base;

public interface BaseEnum {

    Integer getCode() ;

    String getMessage() ;

}