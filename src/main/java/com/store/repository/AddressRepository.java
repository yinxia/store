package com.store.repository;

import com.store.base.BaseRepository;
import com.store.entity.Address;

/**
 * @ClassName AddressRepository
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/5 0005 11:04
 * @Version 1.0
 */

public interface AddressRepository extends BaseRepository<Address,Long> {

}
