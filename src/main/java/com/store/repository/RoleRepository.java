package com.store.repository;

import com.store.base.BaseRepository;
import com.store.entity.Role;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface RoleRepository extends BaseRepository<Role,Long> {

    @Query("from Role where userId = ?1")
    List<Role> findRoles(Long userId);
}
