package com.store.repository;

import com.store.base.BaseRepository;
import com.store.entity.Permissions;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;


/**
 * @ClassName PermissionsRepository
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/16 0016 17:14
 * @Version 1.0
 */

public interface PermissionsRepository extends BaseRepository<Permissions,Long> {

    @Query("from Permissions where roleId = ?1")
    List<Permissions> findPermissionsNames(Long roleId);

}
