package com.store.repository;

import com.store.base.BaseRepository;
import com.store.entity.RoleOrMenu;

import java.util.List;

/**
 * @ClassName RoleOrMenuRepository
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/29 0029 15:04
 * @Version 1.0
 */

public interface RoleOrMenuRepository extends BaseRepository<RoleOrMenu,Long> {

    List<RoleOrMenu> findByRoleId(Long roleId);
}
