package com.store.repository;

import com.store.base.BaseRepository;
import com.store.entity.Menu;

/**
 * @ClassName MenuRepository
 * @function
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/6/29 0029 15:03
 * @Version 1.0
 */

public interface MenuRepository extends BaseRepository<Menu,Long> {

}
