package com.store.repository;

import com.store.base.BaseRepository;
import com.store.entity.Wares;

public interface WaresRepository extends BaseRepository<Wares,Long> {

}
