package com.store.repository;

import com.store.base.BaseRepository;
import com.store.entity.InfoUploadFile;

/**
 * @ClassName InfoUploadFileService
 * @Description TODO
 * @Author pengyongliang
 * @Date 2020/5/27 0027 10:51
 * @Version 1.0
 */

public interface InfoUploadFileRepository extends BaseRepository<InfoUploadFile,Long> {




}
