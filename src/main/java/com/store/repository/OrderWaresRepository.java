package com.store.repository;

import com.store.base.BaseRepository;
import com.store.entity.OrderWares;

import java.util.List;

public interface OrderWaresRepository extends BaseRepository<OrderWares,Long> {


    List<OrderWares> findByOrderId(Long orderId);

}
