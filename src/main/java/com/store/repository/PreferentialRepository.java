package com.store.repository;

import com.store.base.BaseRepository;
import com.store.entity.Preferential;

public interface PreferentialRepository extends BaseRepository<Preferential,Long> {
}
