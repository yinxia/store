package com.store.repository;

import com.store.entity.StoreUser;
import com.store.base.BaseRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;

/**
 * @ClassName: StoreUserRepository
 * @Author:
 * @Date: 19-6-27 下午2:46
 * @Desription: ${}
 * @Copy: ${com.bjike}
 **/
public interface StoreUserRepository extends BaseRepository<StoreUser,Long> {


    StoreUser findByPhone(String phone);

    @Modifying
    @Query(" update StoreUser set loginTime = ?1 where phone = ?2 ")
    int updateLoginTime(Date loginTime, String phone);
}
