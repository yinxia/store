package com.store.repository;

import com.store.base.BaseRepository;
import com.store.entity.StoreRoundImg;

public interface StoreRoundImgRepository extends BaseRepository<StoreRoundImg,Long> {

}
