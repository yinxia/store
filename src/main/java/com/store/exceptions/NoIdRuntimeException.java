package com.store.exceptions;

public class NoIdRuntimeException extends  RuntimeException {
   
	private static final long serialVersionUID = -1577476573844070507L;

	public NoIdRuntimeException() {
    }

    public NoIdRuntimeException(String message) {
        super(message);
    }

    public NoIdRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoIdRuntimeException(Throwable cause) {
        super(cause);
    }

    public NoIdRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
