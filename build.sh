#!/bin/bash

profile=$1
echo profile:$profile

isPro=`echo $profile | grep pro`

echo $isPro

if [ -n "$isPro" ] ; then
    echo "copy etc/application.yml"
    /usr/bin/cp etc/application.yml src/main/resources/ ;
fi

mvn package -DskipTests -Djar

docker rm -f store

docker rmi -f store

docker build -t store -f Dockerfile .

if [ -d "/data/store/" ];then
	echo "/data/store/"
else
	mkdir -p /data/store/

chmod 777 /data/store
fi


docker run -d -p 8092:8092  -v /data/store:/data/store --name store store
